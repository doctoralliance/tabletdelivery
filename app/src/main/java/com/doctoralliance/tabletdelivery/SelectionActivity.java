package com.doctoralliance.tabletdelivery;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.doctoralliance.tabletdelivery.Adapters.PhysicianAdapter;
import com.doctoralliance.tabletdelivery.Service.AppService;
import com.doctoralliance.tabletdelivery.ServiceModel.AuthToken;
import com.doctoralliance.tabletdelivery.ServiceModel.PhysicianInfo;
import com.doctoralliance.tabletdelivery.Utils.KioskUtil;

import java.nio.channels.SelectableChannel;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
public class SelectionActivity extends AppCompatActivity {
    AppService appService=null;
    private static int save = -1;
    Button selectButton;
    RelativeLayout mailLayout;
    RelativeLayout confirmLayout;
    TextView physicianName;
    AuthToken auth;
    PhysicianInfo currentPhysician;
    Thread physicianThread;
    TextView txtLastUpdated;
    LinearLayout uppdateContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appService=new AppService(getApplicationContext());
        setContentView(R.layout.activity_selection);
        KioskUtil.Init(SelectionActivity.this);
        Intent i=getIntent();
        auth=(AuthToken) i.getSerializableExtra("auth");



        selectButton=(Button)findViewById(R.id.button_select);
        mailLayout=(RelativeLayout) findViewById(R.id.selectction_main);
        confirmLayout=(RelativeLayout) findViewById(R.id.selectction_confirm);
        physicianName=(TextView)findViewById(R.id.physician_name);
        txtLastUpdated=(TextView)findViewById(R.id.txtLastUpdated);
        uppdateContainer=(LinearLayout)findViewById(R.id.uppdateContainer);
        mailLayout.setVisibility(View.VISIBLE);
        confirmLayout.setVisibility(View.GONE);
        selectButton.setClickable(false);
        selectButton.setEnabled(false);
        setTitle(auth.getCompany() +" Tablet Delivery");

       // InitThread();

        getPhysicians();
    }

    private void getPhysicians(){



        physicianThread = new Thread(new Runnable() {

            @Override
            public void run() {


                Populatephysician(auth);


            }});
        physicianThread.start();


    }
    private void setTitle(String titile){
        TextView t=(TextView)findViewById(R.id.app_title);
        TextView t1=(TextView)findViewById(R.id.app_title1);
        t.setText(titile);
        t1.setText(titile);
    }
    public void Select_Onclick(View v){
        if(currentPhysician!=null){
            physicianName.setText(currentPhysician.Name);
            mailLayout.setVisibility(View.GONE);
            confirmLayout.setVisibility(View.VISIBLE);
        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        KioskUtil.hideSystemUI(getWindow().getDecorView());
    }

    @Override
    public void onBackPressed() {

    }
    public void Update_OnClick(View v){
        getPhysicians();
    }
    public void logout_Onclick(View v){
        currentPhysician=null;
        Intent loginIntent=new Intent(SelectionActivity.this,LoginActivity.class);
        loginIntent.putExtra("type","1");
        startActivity(loginIntent);
    }
    public void cancel_Onclick(View v){
        confirmLayout.setVisibility(View.GONE);
        mailLayout.setVisibility(View.VISIBLE);

    }
    public void confirm_Onclick(View v){
        Intent syncIntent=new Intent(SelectionActivity.this,SyncActivity.class);
        syncIntent.putExtra("synctype","0");
        syncIntent.putExtra("auth",auth);
        syncIntent.putExtra("physician",currentPhysician);
        startActivity(syncIntent);
    }
    public void Populatephysician(AuthToken token){

       final List<PhysicianInfo> physicians=appService.getPhysicians(token);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                uppdateContainer.setVisibility(View.GONE);
           ListView list=(ListView)findViewById(R.id.physician_list);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        currentPhysician= (PhysicianInfo) parent.getAdapter().getItem(position);
                        selectButton.setClickable(true);
                        selectButton.setEnabled(true);
                    }
                });
                list.setAdapter(new PhysicianAdapter(getApplicationContext(),physicians));

                Calendar c = Calendar.getInstance();
                System.out.println("Current time =&gt; "+c.getTime());

                SimpleDateFormat df = new SimpleDateFormat("MMM dd, hh:mm a");
                String formattedDate = df.format(c.getTime());


                uppdateContainer.setVisibility(View.VISIBLE);
                txtLastUpdated.setText("Last Updated : " +formattedDate );
            }
        });

    }

    public void onVideoClick(View v){
        Intent selectionIntent=new Intent(SelectionActivity.this,PlayerActivity.class);
        startActivity(selectionIntent);
    }

    public void onPresentationClick(View v){
        Intent presentationIntent=new Intent(SelectionActivity.this,PresentationActivity.class);
        startActivity(presentationIntent);
    }

}
