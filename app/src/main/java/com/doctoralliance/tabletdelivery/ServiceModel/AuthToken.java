package com.doctoralliance.tabletdelivery.ServiceModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ansad on 01-06-2018.
 */

public class AuthToken implements Serializable {
    @SerializedName("access_token")
    public String AccessToken;
    @SerializedName("token_type")
    public String Tokentype;
    @SerializedName("expires_in")
    public long ExpiresIn;
    @SerializedName("refresh_token")
    public String RefreshToken;
    @SerializedName("userName")
    public String Username;
    @SerializedName("userType")
    public String UserType;

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public  String Company;

    public Boolean isValid(){
        if(Error!=null){
            return false;
        }
        else return true;
    }
    public String getError() {
        return Error;
    }

    public void setError(String error) {
        Error = error;
    }

    @SerializedName("error_description")
    public String Error;

    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }

    public String getTokentype() {
        return Tokentype;
    }

    public void setTokentype(String tokentype) {
        Tokentype = tokentype;
    }

    public long getExpiresIn() {
        return ExpiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        ExpiresIn = expiresIn;
    }

    public String getRefreshToken() {
        return RefreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        RefreshToken = refreshToken;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }
}
