package com.doctoralliance.tabletdelivery;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.drm.DrmStore;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.doctoralliance.tabletdelivery.Receiver.IPowerReceiverCallback;
import com.doctoralliance.tabletdelivery.Receiver.PowerConnectionReceiver;
import com.doctoralliance.tabletdelivery.Service.AppService;
import com.doctoralliance.tabletdelivery.Service.DocumentService;
import com.doctoralliance.tabletdelivery.Settings.AppSettings;
import com.doctoralliance.tabletdelivery.Utils.KioskUtil;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

public class ReviewActivity extends AppCompatActivity {

    TextView txtSigned;
    TextView txtRejected;
    TextView txtUnviewed;
    SignaturePad signPad;
    Button btnClear;
    Button btnSign;
    TextView btnBack;
    Button btnGoback;
    long physicianId;
    String physicianName;
    String agencyName;
    LinearLayout pnlunviewd;
    DocumentService docService;
    AppService appService;


    PowerConnectionReceiver powerReceiver;

    PopupWindow popupPower;


    public static Context applicationContext = null;
    public static Thread.UncaughtExceptionHandler defaultHandler = null;
    public static Thread.UncaughtExceptionHandler exceptionHandler = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {

                CommonUtil.ShowError(ReviewActivity.this,paramThrowable);
            }
        });

        setContentView(R.layout.activity_review);
        KioskUtil.Init(ReviewActivity.this);
        appService=new AppService(this);
        docService=new DocumentService(this);

        Init();
    }



    private void Init(){

        //Get Intent Value

        physicianId=appService.getTabPhysicianId();
        physicianName=appService.getTabPhysician();
        agencyName=appService.getTabPAgency();
        setTitle(agencyName + " : " + physicianName + " Documents");

        txtSigned=(TextView)findViewById(R.id.txtSigned);
        txtRejected=(TextView)findViewById(R.id.txtRejected);
        txtUnviewed=(TextView)findViewById(R.id.txtUnviewed);
        btnBack=(TextView)findViewById(R.id.btnBack);
        btnGoback=(Button) findViewById(R.id.btnViewAll);
        btnSign=(Button)findViewById(R.id.btnSign);
        signPad=(SignaturePad)findViewById(R.id.signature_pad);

        pnlunviewd=(LinearLayout)findViewById(R.id.pnlUnviwed);
        pnlunviewd.setVisibility(View.GONE);
        btnBack.setVisibility(View.GONE);
        btnGoback.setVisibility(View.GONE);
        btnSign.setVisibility(View.GONE);

        new LoadDocumentDetailsAsync().execute(physicianId);
    }
    private void setTitle(String title){
        TextView text=(TextView)findViewById(R.id.app_title);
        text.setText(title);

    }

    public void onExitClick(View v){

        try {

            appService.setMarketerMode();
            UnregisterPowerReceiver();
            Intent signIntent = new Intent(ReviewActivity.this, LoginActivity.class);
            signIntent.putExtra("type", "1");
            startActivity(signIntent);
        }
        catch(Exception ex){

            CommonUtil. ShowError(ReviewActivity.this,ex);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        KioskUtil.hideSystemUI(getWindow().getDecorView());
        if(AppSettings.hasPowerDisplayedLow==false){
            powerReceiver=new PowerConnectionReceiver(new ReviewActivity.PowerService());
            registerReceiver(powerReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        UnregisterPowerReceiver();

    }
    private void UnregisterPowerReceiver(){
        if(powerReceiver!=null){
            unregisterReceiver(powerReceiver);
            powerReceiver=null;
        }
    }
    public void ShowBatteryPopup(int percentage){
        if(popupPower!=null)
        {
            popupPower.dismiss();
            popupPower=null;
        }

        LayoutInflater layoutInflater = (LayoutInflater) ReviewActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.pwr_layout,null);

        ImageButton btnPowerClose = (ImageButton) customView.findViewById(R.id.ib_close);
        Button btnpowerOk = (Button) customView.findViewById(R.id.btnCommentDone);

        TextView txtHead=(TextView) customView.findViewById(R.id.txt_Head);
        txtHead.setText("Low battery warning - " + percentage +"%");

        TextView txtContent=(TextView) customView.findViewById(R.id.txt_Content);
        txtContent.setText("Battery is at "+ percentage +"%. Please connect the tablet to be charged.");
        //instantiate popup window
        popupPower = new PopupWindow(customView, RelativeLayout.LayoutParams.WRAP_CONTENT, 200,true);




        //close the popup window on button click
        btnPowerClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupPower.dismiss();
            }
        });
        //close the popup window on button click
        btnpowerOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupPower.dismiss();
            }
        });
        RelativeLayout layoutViewer=(RelativeLayout) findViewById(R.id.main_layout);
        popupPower.showAtLocation(layoutViewer, Gravity.CENTER, 0, 0);
    }
    public class PowerService implements IPowerReceiverCallback {
        @Override
        public void Excecute(int percentage,Boolean isCharging)
        {
            if(!isCharging){
                if(percentage<= AppSettings.batteryCutOffPercentageLow && !AppSettings.hasPowerDisplayedLow){
                    AppSettings.hasPowerDisplayedLow=true;
                    AppSettings.hasPowerDisplayedMed=true;
                    AppSettings.hasPowerDisplayedHigh=true;
                    ShowBatteryPopup(percentage);
                }
                else if(percentage<= AppSettings.batteryCutOffPercentageMed && !AppSettings.hasPowerDisplayedMed){
                    AppSettings.hasPowerDisplayedMed=true;
                    AppSettings.hasPowerDisplayedHigh=true;
                    ShowBatteryPopup(percentage);
                }
                else if(percentage<= AppSettings.batteryCutOffPercentageHigh && !AppSettings.hasPowerDisplayedHigh){
                    AppSettings.hasPowerDisplayedHigh=true;
                    ShowBatteryPopup(percentage);
                }
            }

        }
    }

    @Override
    public void onBackPressed() {

    }
    public void onBackClick(View v){
        Intent intent=new Intent(ReviewActivity.this,PhysicianActivity.class);
        intent.putExtra("doc","1");
        startActivity(intent);
    }
    public void onClearClick(View v){
        signPad.clear();
    }
    public void onConfirmClick(View v){

        String signature=getSignature();
        appService.CompletePhysicingSign(signature);

        Intent signIntent=new Intent(ReviewActivity.this,SignActivity.class);
        startActivity(signIntent);
    }
    private String getSignature(){
        Bitmap sign=signPad.getSignatureBitmap();

        return encodeToBase64(sign);
    }
    public static String encodeToBase64(Bitmap image)
    {
        Bitmap immagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }
    private void LoadData(long signed,long rejeted,long unviewed){

        txtUnviewed.setText(unviewed+"");
        txtRejected.setText(rejeted+"");
        txtSigned.setText(signed+"");
        if(unviewed>0){
            pnlunviewd.setVisibility(View.VISIBLE);
            btnGoback.setVisibility(View.VISIBLE);
        }
        else{
            btnSign.setVisibility(View.VISIBLE);
            btnBack.setVisibility(View.VISIBLE);
        }



    }

    private class LoadDocumentDetailsAsync extends AsyncTask<Long, Integer, Long> {
        protected Long doInBackground(Long... physicianId) {
            LoadData(docService.GetSignedCount(physicianId[0]),docService.GetRejecteddCount(physicianId[0]),docService.GetUnvieweddCount(physicianId[0]));
            return 0l;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Long result) {

        }
    }
}
