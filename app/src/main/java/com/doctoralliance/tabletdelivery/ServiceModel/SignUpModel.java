package com.doctoralliance.tabletdelivery.ServiceModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ansad on 01-24-2018.
 */

public class SignUpModel extends TabletInfo {
    @SerializedName("physicianId")
    public long  PhysicianId ;


    public long getPhysicianId() {
        return PhysicianId;
    }

    public void setPhysicianId(long physicianId) {
        PhysicianId = physicianId;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }
    @SerializedName("contact")
    public String Contact ;
}
