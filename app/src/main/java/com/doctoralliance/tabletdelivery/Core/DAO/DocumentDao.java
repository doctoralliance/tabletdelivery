package com.doctoralliance.tabletdelivery.Core.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.doctoralliance.tabletdelivery.Core.Entity.Document;

import java.util.List;

@Dao
public interface DocumentDao  {

    /*Get all documents by Physician Id*/
    @Query("SELECT * FROM Document Where physicianId= :physicianId order by Id")
    List<Document> GetList(long physicianId);

    /*Get all documents by Physician Id*/
    @Query("SELECT Id FROM Document Where physicianId= :physicianId order by Id")
    long[] GetIdList(long physicianId);

    /*Get a document by Id*/
    @Query("SELECT * FROM Document Where id= :id")
    Document Get(long id);

    /*Get a document by docId*/
    @Query("SELECT * FROM Document Where docId= :docId")
    Document GetByDocId(long docId);

    /*Count of Documents by physicianID*/
    @Query("SELECT COUNT(*) FROM Document Where physicianId= :physicianId")
    long count(long physicianId);
    /*Count of Documents by physicianID*/
    @Query("SELECT COUNT(*) FROM Document Where physicianId= :physicianId and status=:status")
    long countbystatus(long physicianId,String status);

    /*Count of Documents by physicianID*/
    @Query("SELECT COUNT(*) FROM Document Where physicianId!= :physicianId")
    long unwntedDocCount(long physicianId);

    /*Add a document*/
    @Insert
    long Add(Document document);

    /*Delete by id*/
    @Query("DELETE FROM Document Where id= :id")
    int Delete(long id);

    /*Delete by docId*/
    @Query("DELETE FROM Document Where docId= :docId")
    int DeleteByDocId(long docId);

    /*Delete by docId*/
    @Query("DELETE FROM Document")
    int Clean();

    /*Update Document by id, for SignedDate, Status, Comments*/
    @Query("Update Document SET signedDate= :signDate, status= :status, comments= :comments WHERE id= :id ")
    void Update(long id, long signDate, String status, String comments);
}
