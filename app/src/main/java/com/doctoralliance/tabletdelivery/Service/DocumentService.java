package com.doctoralliance.tabletdelivery.Service;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;
import android.content.Context;
import android.support.annotation.Nullable;

import com.doctoralliance.tabletdelivery.Core.Database.AppDatabase;
import com.doctoralliance.tabletdelivery.Core.Entity.Document;

import java.io.File;
import java.util.Date;
import java.util.List;

public class DocumentService extends BaseService {

    FileService fileService;

    public DocumentService(Context context){
        super(context);
        fileService=new FileService(context);


        }

    public long[] getDocids(long physicianid){
            return db.DocumentDao().GetIdList(physicianid);
    }

    private Document addDocument(long docId, long physicianId, String physicianName, long patientId, String patientName, String careProvider, String docType, String docPath, String status, String comments, long sentDate, long signedDate){

        Document document = new Document();

        document.setDocId(docId);
        document.setPhysicianId(physicianId);
        document.setPhysicianName(physicianName);
        document.setPatientName(patientName);
        document.setPatientId(patientId);
        document.setStatus(status);
        document.setDocPath(docPath);
        document.setDocType(docType);
        document.setSentDate(sentDate);
        document.setCareProvider(careProvider);
        document.setComments(comments);
        document.setSignedDate(signedDate);
        db.DocumentDao().Add(document);
        return document;
    }
    public Document Save(long docId, long physicianId, String physicianName, long patientId,
                         String patientName, String careProvider, String docType,
                         Date sentDate,
                          byte[] buffer)
    {

        long date=0;
        if(sentDate!=null)
            date=sentDate.getTime();

        File file=fileService.Create(docId+".pdf",buffer);
        if(file!=null){
            Document doc= addDocument( docId,  physicianId,  physicianName,  patientId,
                    patientName,  careProvider,  docType,
                    file.getAbsolutePath(),"0",
                    "",
                    date,
                    0);
            return doc;
        }
        return null;
    }

    private List<Document> getDocumentsByPhysician(final AppDatabase db, long physicianId) {
        return db.DocumentDao().GetList(physicianId);
    }
private long GetCountByStatus(long PhysicianId,String status){
    return db.DocumentDao().countbystatus(PhysicianId,status);
}

    public long GetSignedCount(long PhysicianId){
        return GetCountByStatus(PhysicianId,"1");
    }
    public long GetRejecteddCount(long PhysicianId){
        return GetCountByStatus(PhysicianId,"2");
    }
    public long GetUnvieweddCount(long PhysicianId){
        return GetCountByStatus(PhysicianId,"0");
    }

    public Document get(long id){
        return db.DocumentDao().Get(id);
    }

    public Document getByDocId(long docId){
        return db.DocumentDao().GetByDocId(docId);
    }

    public long countByPhysician(long physicianId){
        return db.DocumentDao().count(physicianId);
    }

    public int delete(long id) {
        return db.DocumentDao().Delete(id);
    }
    public int delete(Document doc ) {
        if(doc!=null){
            fileService.Delete(doc.getDocPath());
        }
        return db.DocumentDao().Delete(doc.id);
    }


    public int deleteByDocId(long docId) {
        return db.DocumentDao().DeleteByDocId(docId);
    }


    public void UpdateStatus(long Id,Date signDate,String comments,String Status){
        db.DocumentDao().Update(Id, signDate.getTime(), Status, comments);
    }
    public void CleanForPhysician(long physicianId){
        long count= db.DocumentDao().unwntedDocCount(physicianId);
        if(count>0){
            try{
                fileService.CleanFolder();
            }catch(Exception ex){

            }

            db.DocumentDao().Clean();

        }
    }
}
