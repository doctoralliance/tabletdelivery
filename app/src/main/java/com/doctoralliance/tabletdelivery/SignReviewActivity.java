package com.doctoralliance.tabletdelivery;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.doctoralliance.tabletdelivery.Service.AppService;
import com.doctoralliance.tabletdelivery.Service.DocumentService;
import com.doctoralliance.tabletdelivery.ServiceModel.AuthToken;
import com.doctoralliance.tabletdelivery.ServiceModel.PhysicianInfo;
import com.doctoralliance.tabletdelivery.Utils.KioskUtil;

import org.w3c.dom.Text;

public class SignReviewActivity extends AppCompatActivity {

    AuthToken token;
    AppService appService;
    DocumentService docService;
    PhysicianInfo physician;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_review);
        KioskUtil.Init(SignReviewActivity.this);
        appService=new AppService(SignReviewActivity.this);
        token=(AuthToken) getIntent().getSerializableExtra("auth");
        physician=appService.getSyncPhysician();

        docService=new DocumentService(SignReviewActivity.this);
        initPage();
    }
    private void initPage(){
        setTitle();
        TextView signedText=(TextView)findViewById(R.id.txtSigned);
        TextView unSignedText=(TextView)findViewById(R.id.txtUnsigned);
        TextView txtphysician=(TextView)findViewById(R.id.physician);
        signedText.setText("Signed Documents : "+ docService.GetSignedCount(physician.Id) );
        unSignedText.setText("UnSigned Documents : "+ docService.GetRejecteddCount(physician.Id) );
        txtphysician.setText(physician.Name + " Overview");
    }
    private void setTitle(){
        TextView v=(TextView)findViewById(R.id.app_title);
        v.setText(token.getCompany()+ " Tablet Delivery");
    }
    public void onSyncClick(View v){

        Intent syncIntent=new Intent(SignReviewActivity.this,SyncActivity.class);
        syncIntent.putExtra("synctype","1");
        syncIntent.putExtra("auth",token);
        syncIntent.putExtra("physician",appService.getSyncPhysician());
        startActivity(syncIntent);
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        KioskUtil.hideSystemUI(getWindow().getDecorView());
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        KioskUtil.hideSystemUI(getWindow().getDecorView());
                    }
                });
    }
    @Override
    public void onBackPressed() {

    }
}
