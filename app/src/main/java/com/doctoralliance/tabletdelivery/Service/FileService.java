package com.doctoralliance.tabletdelivery.Service;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileService extends BaseService {


    public FileService(Context context){
        super(context);

    }
    private File getStoragePath(){
         File basePath;
        basePath=getContext().getFilesDir();
        File storagePath=new File(basePath,"/documents");
        if(!storagePath.exists()){
            storagePath.mkdirs();
        }
        return storagePath;
    }
public void CleanFolder(){
    File baseStorage=getStoragePath();
    if (baseStorage.exists() && baseStorage.isDirectory())
    {
        String[] children = baseStorage.list();
        for (int i = 0; i < children.length; i++)
        {
            new File(baseStorage, children[i]).delete();
        }
    }
}
public void Delete(String path){


    File file=new File(path);
    if(file.exists()){
        file.delete();
    }
}
    public File Create(String filename, byte[] buffer) {

        File baseStorage=getStoragePath();

        File file=new File(baseStorage,filename);

        if (file.exists()) {
           file.delete();
        }
        FileOutputStream fileOuputStream = null;
        try {
            fileOuputStream = new FileOutputStream(file);
            fileOuputStream.write(buffer);
            fileOuputStream.close();

            return file;
        } catch (IOException e) {
            e.printStackTrace();

            return null;
        } finally {
            if (fileOuputStream != null) {
                fileOuputStream = null;
            }
        }
    }


}
