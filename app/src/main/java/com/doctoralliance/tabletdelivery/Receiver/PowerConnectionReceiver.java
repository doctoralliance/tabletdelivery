package com.doctoralliance.tabletdelivery.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.BatteryManager;

/**
 * Created by ansad on 02-08-2018.
 */

public class PowerConnectionReceiver extends BroadcastReceiver {
    IPowerReceiverCallback callback=null;


    public PowerConnectionReceiver(IPowerReceiverCallback _callback){
        callback=_callback;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int batteryLevel = intent.getIntExtra(
                BatteryManager.EXTRA_LEVEL, 0);
        int maxLevel = intent
                .getIntExtra(BatteryManager.EXTRA_SCALE, 0);
        int batteryHealth = intent.getIntExtra(
                BatteryManager.EXTRA_HEALTH,
                BatteryManager.BATTERY_HEALTH_UNKNOWN);
        int batteryPercentage =Math.round (((float)batteryLevel/ (float)maxLevel) * 100);

        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;


        if(callback!=null){
            callback.Excecute(batteryPercentage,isCharging);
        }
    }
}
