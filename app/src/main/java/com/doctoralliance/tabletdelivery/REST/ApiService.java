package com.doctoralliance.tabletdelivery.REST;
import com.doctoralliance.tabletdelivery.Service.AppService;
import com.doctoralliance.tabletdelivery.ServiceModel.AppInfo;
import com.doctoralliance.tabletdelivery.ServiceModel.AuthToken;
import com.doctoralliance.tabletdelivery.ServiceModel.DocumentModel;
import com.doctoralliance.tabletdelivery.ServiceModel.PhysicianInfo;
import com.doctoralliance.tabletdelivery.ServiceModel.RejectModel;
import com.doctoralliance.tabletdelivery.ServiceModel.Result;
import com.doctoralliance.tabletdelivery.ServiceModel.SignModel;
import com.doctoralliance.tabletdelivery.ServiceModel.SignUpModel;
import com.doctoralliance.tabletdelivery.ServiceModel.SignatreModel;
import com.doctoralliance.tabletdelivery.ServiceModel.TabletInfo;
import com.doctoralliance.tabletdelivery.ServiceModel.UserModel;
import com.doctoralliance.tabletdelivery.Settings.AppSettings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by ansad on 01-11-2018.
 */

public class ApiService {
    DAInterface client;
    Gson gson;
    public ApiService(){

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();

        OkHttpClient okclient = new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120,TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppSettings.getApiURL()).client(okclient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        client= retrofit.create(DAInterface.class);
        gson=new Gson();
    }
    public AuthToken GetAuthToken(String usernam, String password){
        AuthToken response=null;
        try{

            Response<AuthToken> res= client.getAuthtoken("password",usernam,password,"user").execute();
            response=res.body();
            if(response==null){
                response=new AuthToken();
                response.setError("Invalid Username or password");
            }
        }catch(Exception ex){
            response=new AuthToken();
            response.setError(ex.getMessage());
        }
        return  response;
    }
    public Result<List<PhysicianInfo>> GetPhysicians(String authHeader){
        Result<List<PhysicianInfo>> response=null;
        try{

            Response<Result<List<PhysicianInfo>>> res= client.getPhysicians(authHeader).execute();
            response=res.body();

        }catch(Exception ex){
            response=new Result<List<PhysicianInfo>>(ex.getMessage());

        }
        return  response;
    }

    public Result<UserModel> GetMe(String authHeader){
        Result<UserModel> response=null;
        try{

            Response<Result<UserModel>> res= client.getMe(authHeader).execute();
            response=res.body();

        }catch(Exception ex){
            response=new Result<UserModel>(ex.getMessage());
        }
        return  response;
    }
    public Result<TabletInfo> RegisterTablet(TabletInfo tablet, String usernam, String password){
        Result<TabletInfo> response=null;
        try{
            Response<Result<TabletInfo>> res= client.Register(tablet,usernam,password).execute();
            response=res.body();
        }catch(Exception ex){
            return new Result<TabletInfo>(ex.getMessage());
        }
        return  response;
    }
    public Result<AppInfo> GetVersionInfo(){
        Result<AppInfo> response=null;
        try{
            Response<Result<AppInfo>> res= client.GetVersionInfo().execute();
            response=res.body();
        }catch(Exception ex){
            return new Result<AppInfo>(ex.getMessage());
        }
        return  response;
    }
    public Result<Boolean> Sign(long docId,Date date,long tabId,String fingerprint,String authHeader){
        SignModel model=new SignModel();

       // model.PhysicianId=physicianid;
        model.Id=tabId;
        model.setDocId(docId);
        model.setFingerPrint(fingerprint);
        model.setSignDate(date);
        model.setDocId(docId);

        //model.setSignaturebas64(signatureBase64);

        Result<Boolean> response=null;
        try{
            Response<Result<Boolean>> res= client.sign(authHeader,model).execute();
            response=res.body();
        }catch(Exception ex){
            return new Result<Boolean>(ex.getMessage());
        }
        return  response;
    }
    public Result<Boolean> Reject(long docId,Date date,long tabId,String fingerprint,String authHeader,String comment){
        RejectModel model=new RejectModel();

        // model.PhysicianId=physicianid;
        model.Id=tabId;
        model.setDocId(docId);
        model.setFingerPrint(fingerprint);
        model.setRejectDate(date);
        model.setComments(comment);
        //model.setDocId(docId);

        //model.setSignaturebas64(signatureBase64);

        Result<Boolean> response=null;
        try{
            Response<Result<Boolean>> res= client.reject(authHeader,model).execute();
            response=res.body();
        }catch(Exception ex){
            return new Result<Boolean>(ex.getMessage());
        }
        return  response;
    }

    public Result<Boolean> SaveSignature(String authHeader ,long physicianId,long tabId,String fingerprint,String signature){


        SignatreModel model=new SignatreModel();

        // model.PhysicianId=physicianid;
        model.Id=tabId;

        model.setFingerPrint(fingerprint);
        model.setPhysicianId(physicianId);
        model.setSignaturebas64(signature);
        //model.setDocId(docId);

        //model.setSignaturebas64(signatureBase64);

        Result<Boolean> response=null;
        try{
            Response<Result<Boolean>> res= client.saveSignature(authHeader,model).execute();
            response=res.body();
        }catch(Exception ex){
            return new Result<Boolean>(ex.getMessage());
        }
        return  response;
    }
    public Result<Boolean> SignUp(String authHeader ,long physicianId,long tabId,String fingerprint,String contact){


        SignUpModel model=new SignUpModel();

        // model.PhysicianId=physicianid;
        model.Id=tabId;

        model.setFingerPrint(fingerprint);
        model.setPhysicianId(physicianId);
        model.setContact(contact);

        Result<Boolean> response=null;
        try{
            Response<Result<Boolean>> res= client.signup(authHeader,model).execute();
            response=res.body();
        }catch(Exception ex){
            return new Result<Boolean>(ex.getMessage());
        }
        return  response;
    }


    public Result<long[]> GetDocumentIds(String authHeader,long physicianid){
        Result<long[]> response=null;
        try{
            Response<Result<long[]>> res= client.getDocumentIds(authHeader,physicianid).execute();
            response=res.body();
        }catch(Exception ex){
            return new Result<long[]>(ex.getMessage());
        }
        return  response;
    }
    public Result<DocumentModel> GetDocument(String authHeader, long docid){
        Result<DocumentModel> response=null;
        try{
            Response<Result<DocumentModel>> res= client.getDocument(authHeader,docid).execute();
            response=res.body();
        }catch(Exception ex){
            return new Result<DocumentModel>(ex.getMessage());
        }
        return  response;
    }
}

