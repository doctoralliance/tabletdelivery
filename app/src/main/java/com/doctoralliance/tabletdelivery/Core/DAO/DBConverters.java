package com.doctoralliance.tabletdelivery.Core.DAO;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Created by ansad on 01-08-2018.
 */

public class DBConverters {
    @TypeConverter
    public Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public Long dateToTimestamp(Date date) {
        if (date == null) {
            return null;
        } else {
            return date.getTime();
        }
    }
}