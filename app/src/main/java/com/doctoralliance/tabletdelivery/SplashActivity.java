package com.doctoralliance.tabletdelivery;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;


import com.doctoralliance.tabletdelivery.Service.AppService;
import com.doctoralliance.tabletdelivery.Utils.KioskUtil;
import com.jakewharton.processphoenix.ProcessPhoenix;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends Activity {

    AppService _appService;
    private static final int MY_PERMISSIONS_REQUEST =1 ;
    public final static int REQUEST_CODE = -1010101;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        _appService=new AppService(getApplicationContext());
        //verifyStoragePermissions(SplashActivity.this);
        KioskUtil.Init(SplashActivity.this);


        getPermissionAndMove();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //check if received result code
        //  is equal our requested code for draw permission
        if (requestCode == REQUEST_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {

                }
            }
        }
        getPermissionAndMove();
    }
    private void StartApp(){
        if(_appService.isKioskEnabled(SplashActivity.this)){
            KioskUtil.EnableKioskMod(SplashActivity.this);
        }
        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                InitializeApp();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void getPermissionAndMove() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (!Settings.canDrawOverlays(SplashActivity.this)) {

                /** if not construct intent to request permission**/
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getApplicationContext().getPackageName()));
                /* request permission via start activity for result */
                issBack = true;
                startActivityForResult(intent, REQUEST_CODE); //It will call onActivityResult Function After you press Yes/No and go Back after giving permission

            }
            else{
                if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE ) != PackageManager.PERMISSION_GRANTED) {


                    ActivityCompat.requestPermissions(SplashActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE }, MY_PERMISSIONS_REQUEST);

                }

                else{

                    StartApp();
                }
            }
        }



    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == MY_PERMISSIONS_REQUEST) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                StartApp();

            }else{
                Toast.makeText(SplashActivity.this, "permission not granted and cannot start the application", Toast.LENGTH_SHORT).show();
                //ProcessPhoenix.triggerRebirth(getApplicationContext());
                return;
            }
        }
    }

    private Boolean isLocationEnabled(){
        LocationManager locationManager=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
       return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
    private void InitTabletRegister(){
        StartIntent("0",LoginActivity.class);
    }
    private void InitMarketerLogin(){
        StartIntent("1",LoginActivity.class);
    }
    private void StartIntent(String value,Class<?> startClass ){
        Intent loginIntent = new Intent(SplashActivity.this,startClass);
        loginIntent.putExtra("type", value); //Optional parameters
        SplashActivity.this.startActivity(loginIntent);
    }

    private void InitializeApp(){

        SetText(R.id.status_id,"Checking registration");
        if(!_appService.isRegistered()){
            InitTabletRegister();
        }
        else{
            if(_appService.isPhsicianLogin()){
                Intent physicianIntent=new Intent(SplashActivity.this,PhysicianActivity.class);
                startActivity(physicianIntent);
            }
            else{
                InitMarketerLogin();
            }
        }
    }
    private Boolean issBack=false;
    @Override
    protected void onResume() {
        super.onResume();
        KioskUtil.hideSystemUI(getWindow().getDecorView());

    }

    @Override
    public void onBackPressed() {
        getPermissionAndMove();
    }
    private void SetText(int id,String text){

        TextView tvName = (TextView)findViewById(id);
        tvName.setText(text);
    }
}
