package com.doctoralliance.tabletdelivery.Receiver;

/**
 * Created by ansad on 01-06-2018.
 */

public class NetworkStateChanged {
    private Boolean mIsInternetConnected;

    public NetworkStateChanged(boolean isInternetConnected) {
        this.mIsInternetConnected = isInternetConnected;
    }

    public boolean isInternetConnected() {
        return this.mIsInternetConnected;
    }
}
