package com.doctoralliance.tabletdelivery;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.doctoralliance.tabletdelivery.Utils.KioskUtil;

import java.io.File;

import static com.doctoralliance.tabletdelivery.Utils.KioskUtil.DisableKioskMode;

public class AutoUpdateActivity extends AppCompatActivity {

    BroadcastReceiver onComplete;
     DownloadManager manager;
    long downloadId;
    TextView txttProgress;
    TextView txtHeader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_update);
        String url=getIntent().getStringExtra("url");
        String version=getIntent().getStringExtra("version");
        KioskUtil.Init(AutoUpdateActivity.this);
        txttProgress=(TextView)findViewById(R.id.txtProgress);
        txtHeader=(TextView)findViewById(R.id.txtHeader);
        Update(url,version);
    }
    @Override
    protected void onResume() {
        super.onResume();
        KioskUtil.hideSystemUI(getWindow().getDecorView());
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        KioskUtil.hideSystemUI(getWindow().getDecorView());
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        KioskUtil.hideSystemUI(getWindow().getDecorView());
                    }
                });
    }
    @Override
    public void onBackPressed() {

    }
    private void SetHeader(String text){

        txtHeader.setText(text);
    }
    private void Update(String url, final String version){
        //get destination to update file and set Uri
        //TODO: First I wanted to store my update .apk file on internal storage for my app but apparently android does not allow you to open and install
        //aplication with existing package from there. So for me, alternative solution is Download directory in external storage. If there is better
        //solution, please inform us in comment
        final String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/DA-Tablet-Delivery.apk";

        final Uri uri = Uri.parse("file://" + destination);

        //Delete update file if exists
        File file = new File(destination);
        if (file.exists())
            //file.delete() - test this, I think sometimes it doesnt work
            file.delete();



        //set downloadmanager
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("Downloading new version ("+ version +")");
        request.setTitle(getString(R.string.app_name));

        //set destination
        request.setDestinationUri(uri);

        // get download service and enqueue file
         manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
         downloadId = manager.enqueue(request);
         SetHeader("Downloading");
        DownloadProgress();
        //set BroadcastReceiver to install app when .apk is downloaded
         onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {

                unregisterReceiver(this);
                finish();

                Install(uri,destination,version);

               // finishAndRemoveTask();
            }
        };
        //register receiver for when .apk download is compete
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }
    private void DownloadProgress(){

        new Thread(new Runnable() {

            @Override
            public void run() {

                boolean downloading = true;

                while (downloading) {

                    DownloadManager.Query q = new DownloadManager.Query();
                    q.setFilterById(downloadId);

                    Cursor cursor = manager.query(q);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor
                            .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false;
                    }

                    final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            txttProgress.setText( "Progress: "+ (int) dl_progress + "%");

                        }
                    });
                    cursor.close();
                }

            }
        }).start();
    }
    private void Install(Uri uri,String destination,String version){
        File file=new File(destination);

        if(file.exists()){
            SetHeader("Installing " + version);
            DisableKioskMode(AutoUpdateActivity.this);

            Intent install = new Intent(Intent.ACTION_VIEW);
            install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            install.setDataAndType(uri,
                    manager.getMimeTypeForDownloadedFile(downloadId));
            startActivity(install);
        }

    }
    protected void onCancelClick(View v){

        try{

        unregisterReceiver(onComplete);


        }catch(Exception ex){

        }
        finish();

        Intent seletionIntent = new Intent(AutoUpdateActivity.this,LoginActivity.class);
        seletionIntent.putExtra("type","1");

        startActivity( seletionIntent);
    }
}
