package com.doctoralliance.tabletdelivery.Core.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.doctoralliance.tabletdelivery.Core.DAO.DBConverters;

import java.sql.Date;

@Entity(tableName = "Document", indices = {@Index(value = {"docId"}, unique = true)})

public class Document {

    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo(name = "docId")
    public long docId;

    @ColumnInfo(name = "physicianName")
    public String physicianName;

    @ColumnInfo(name = "patientName")
    public String patientName;

    @ColumnInfo(name = "careProvider")
    public String careProvider;

    @ColumnInfo(name = "physicianId")
    public long physicianId;

    @ColumnInfo(name = "patientId")
    public long patientId;

    @ColumnInfo(name = "comments")
    public String comments;

    //Status(Synced,ToBeSynced,None)
    @ColumnInfo(name = "status")
    public String status;


    @ColumnInfo(name = "sentDate")
    public long sentDate;


    @ColumnInfo(name = "signedDate")
    public long signedDate;

    @ColumnInfo(name = "docPath")
    public String docPath;

    @ColumnInfo(name = "docType")
    public String docType;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDocId() {
        return docId;
    }

    public void setDocId(long docId) {
        this.docId = docId;
    }

    public String getPhysicianName() {
        return physicianName;
    }

    public void setPhysicianName(String physicianName) {this.physicianName = physicianName;}

    public String getCareProvider() {return careProvider;}

    public void setCareProvider(String careProvider) { this.careProvider = careProvider;}

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public long getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(long physicianId) {
        this.physicianId = physicianId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getSentDate() {
        return sentDate;
    }

    public void setSentDate(long sentDate) {
        this.sentDate = sentDate;
    }

    public long getSignedDate() {
        return sentDate;
    }

    public void setSignedDate(long signedDate) {
        this.signedDate = signedDate;
    }

    public String getDocPath() {
        return docPath;
    }

    public void setDocPath(String docPath) {
        this.docPath = docPath;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }
}
