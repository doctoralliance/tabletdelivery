package com.doctoralliance.tabletdelivery.Utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import android.app.admin.DevicePolicyManager;
import android.app.Activity;

import com.doctoralliance.tabletdelivery.Receiver.AdminReceiver;
import com.doctoralliance.tabletdelivery.R;

/**
 * Created by ansad on 01-19-2018.
 */

public class KioskUtil {

    private static View mDecorView;
    private static DevicePolicyManager mDpm;

public static void Init(Context context){
    Activity act=(Activity) context;
    Intent i = ((Activity)context).getIntent();

    ComponentName deviceAdmin = new ComponentName(context, AdminReceiver.class);
    mDpm = (DevicePolicyManager) act.getSystemService(Context.DEVICE_POLICY_SERVICE);
    if (!mDpm.isAdminActive(deviceAdmin)) {

    }

    if (mDpm.isDeviceOwnerApp(act.getPackageName())) {
        mDpm.setScreenCaptureDisabled(deviceAdmin, true);
        mDpm.setLockTaskPackages(deviceAdmin, new String[]{act.getPackageName()});

    } else {
        Toast.makeText(context, act.getString(R.string.not_device_owner), Toast.LENGTH_SHORT).show();
    }

    mDecorView = act.getWindow().getDecorView();
    mDecorView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {

            Rect r = new Rect();
            mDecorView.getWindowVisibleDisplayFrame(r);
            int screenHeight =mDecorView.getRootView().getHeight();

            int keypadHeight = screenHeight - r.bottom;

            //Log.d(TAG, "keypadHeight = " + keypadHeight);

            if (keypadHeight > screenHeight * 0.15) {
                //Keyboard is opened
                hide(mDecorView);
            }
            else {
                // keyboard is closed
            }
        }
    });

    // hideSystemUI(mDecorView,act);
}

public static void DisableKioskMode(Context ctx){

    Activity act=((Activity)ctx);

    act.stopLockTask();
}
public static void EnableKioskMod(Context ctx) {
    boolean lockDevice=true;
        try {
            if (lockDevice) {
                if (mDpm.isLockTaskPermitted(ctx.getPackageName())) {
                    ((Activity)ctx).startLockTask();


                } else {
                    Toast.makeText(ctx, ctx.getString(R.string.kiosk_not_permitted), Toast.LENGTH_SHORT).show();
                }
            } else {

                ((Activity)ctx).startLockTask();
               // mIsKioskEnabled = false;
            }
        } catch (Exception e) {
            // TODO: Log and handle appropriately
        }

    }
    public static void hideSystemUI(final View mDecorView) {



        hide(mDecorView);


    }
    private static void hide(View mDecorView){
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        |View.SYSTEM_UI_FLAG_IMMERSIVE );

    }

}
