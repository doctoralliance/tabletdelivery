package com.doctoralliance.tabletdelivery;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.doctoralliance.tabletdelivery.Receiver.IPowerReceiverCallback;
import com.doctoralliance.tabletdelivery.Receiver.PowerConnectionReceiver;
import com.doctoralliance.tabletdelivery.Service.AppService;
import com.doctoralliance.tabletdelivery.Service.DocumentService;
import com.doctoralliance.tabletdelivery.Settings.AppSettings;
import com.doctoralliance.tabletdelivery.Utils.KioskUtil;

public class SignActivity extends AppCompatActivity {

    long physicianId;
    String physicianName;
    String agencyName;
    DocumentService docService;
    AppService appService;
    CheckBox checkBoxElectronicMode;
    EditText txtContact;


    PowerConnectionReceiver powerReceiver;

    PopupWindow popupPower;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        KioskUtil.Init(SignActivity.this);
        appService=new AppService(this);
        docService=new DocumentService(this);

        physicianId=appService.getTabPhysicianId();
        physicianName=appService.getTabPhysician();
        agencyName=appService.getTabPAgency();
        checkBoxElectronicMode=(CheckBox)findViewById(R.id.checkBoxElectronicMode);
        txtContact=(EditText)findViewById(R.id.txtContact);
        setTitle(agencyName + " : " + physicianName + " Documents");
    }
    public void onVideoClick(View v){
        Intent signIntent=new Intent(SignActivity.this,PlayerActivity.class);
        startActivity(signIntent);
    }

    public void onPresentationClick(View v){
        Intent presentationIntent=new Intent(SignActivity.this,PresentationActivity.class);
        startActivity(presentationIntent);
    }
    public void onCheckBoxCheck(View v){
        txtContact.setEnabled(checkBoxElectronicMode.isChecked());
    }
    public void onReturnClick(View v){

        if(checkBoxElectronicMode.isChecked()){
            appService.SetSendDocElectronically(txtContact.getText().toString());
        }

        Intent signIntent=new Intent(SignActivity.this,LoginActivity.class);
        signIntent.putExtra("type","1");
        startActivity(signIntent);

    }
    @Override
    protected void onResume() {
        super.onResume();
        KioskUtil.hideSystemUI(getWindow().getDecorView());
        if(AppSettings.hasPowerDisplayedLow==false){
            powerReceiver=new PowerConnectionReceiver(new SignActivity.PowerService());
            registerReceiver(powerReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        }
    }



    @Override
    protected void onPause() {
        super.onPause();
        UnregisterPowerReceiver();

    }
    private void UnregisterPowerReceiver(){
        if(powerReceiver!=null){
            unregisterReceiver(powerReceiver);
            powerReceiver=null;
        }
    }


    public void ShowBatteryPopup(int percentage){
        if(popupPower!=null)
        {
            popupPower.dismiss();
            popupPower=null;
        }

        LayoutInflater layoutInflater = (LayoutInflater) SignActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.pwr_layout,null);

        ImageButton btnPowerClose = (ImageButton) customView.findViewById(R.id.ib_close);
        Button btnpowerOk = (Button) customView.findViewById(R.id.btnCommentDone);

        TextView txtHead=(TextView) customView.findViewById(R.id.txt_Head);
        txtHead.setText("Low battery warning - " + percentage +"%");

        TextView txtContent=(TextView) customView.findViewById(R.id.txt_Content);
        txtContent.setText("Battery is at "+ percentage +"%. Please connect the tablet to be charged.");
        //instantiate popup window
        popupPower = new PopupWindow(customView, RelativeLayout.LayoutParams.WRAP_CONTENT, 200,true);




        //close the popup window on button click
        btnPowerClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupPower.dismiss();
            }
        });
        //close the popup window on button click
        btnpowerOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupPower.dismiss();
            }
        });
        RelativeLayout layoutViewer=(RelativeLayout) findViewById(R.id.main_layout);
        popupPower.showAtLocation(layoutViewer, Gravity.CENTER, 0, 0);
    }
    public class PowerService implements IPowerReceiverCallback {
        @Override
        public void Excecute(int percentage,Boolean isCharging)
        {
            if(!isCharging){
                if(percentage<= AppSettings.batteryCutOffPercentageLow && !AppSettings.hasPowerDisplayedLow){
                    AppSettings.hasPowerDisplayedLow=true;
                    AppSettings.hasPowerDisplayedMed=true;
                    AppSettings.hasPowerDisplayedHigh=true;
                    ShowBatteryPopup(percentage);
                }
                else if(percentage<= AppSettings.batteryCutOffPercentageMed && !AppSettings.hasPowerDisplayedMed){
                    AppSettings.hasPowerDisplayedMed=true;
                    AppSettings.hasPowerDisplayedHigh=true;
                    ShowBatteryPopup(percentage);
                }
                else if(percentage<= AppSettings.batteryCutOffPercentageHigh && !AppSettings.hasPowerDisplayedHigh){
                    AppSettings.hasPowerDisplayedHigh=true;
                    ShowBatteryPopup(percentage);
                }
            }

        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        KioskUtil.hideSystemUI(getWindow().getDecorView());
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        KioskUtil.hideSystemUI(getWindow().getDecorView());
                    }
                });
    }
    @Override
    public void onBackPressed() {

    }
}
