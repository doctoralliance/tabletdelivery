package com.doctoralliance.tabletdelivery.Core.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.doctoralliance.tabletdelivery.Core.Entity.Settings;

import java.util.List;

@Dao
public interface SettingsDao {
    @Query("SELECT * FROM Settings")
    List<Settings> getAll();

    @Query("SELECT * FROM Settings where Id = :id")
    Settings findById(long id);

    @Query("SELECT * FROM Settings where Name = :name")
    Settings findByName(String name);

    @Query("SELECT COUNT(*) from Settings")
    long count();

    /*Add a Settings*/
    @Insert
    long Add(Settings settings);

    /*Delete by id*/
    @Query("DELETE FROM Settings Where id= :id")
    int Delete(long id);

    /*Delete by Name*/
    @Query("DELETE FROM Settings Where Name= :name")
    int DeleteByName(String name);

    /*Update Settings, set value by name*/
    @Query("Update Settings SET Value= :value WHERE Name= :name ")
    void Update(String name, String value);
}
