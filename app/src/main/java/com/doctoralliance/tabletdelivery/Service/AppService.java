package com.doctoralliance.tabletdelivery.Service;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.doctoralliance.tabletdelivery.Core.Entity.Settings;
import com.doctoralliance.tabletdelivery.REST.ApiService;

import com.doctoralliance.tabletdelivery.ServiceModel.AuthToken;
import com.doctoralliance.tabletdelivery.ServiceModel.DocumentModel;
import com.doctoralliance.tabletdelivery.ServiceModel.ItemModel;
import com.doctoralliance.tabletdelivery.ServiceModel.PhysicianInfo;
import com.doctoralliance.tabletdelivery.ServiceModel.Result;
import com.doctoralliance.tabletdelivery.ServiceModel.TabletInfo;
import com.doctoralliance.tabletdelivery.ServiceModel.UserModel;
import  com.doctoralliance.tabletdelivery.Settings.AppSettings;

import com.doctoralliance.tabletdelivery.ServiceModel.AppInfo;
import com.doctoralliance.tabletdelivery.Utils.UnlockUtil;

import java.lang.annotation.Retention;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import retrofit2.Response;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Created by ansad on 01-08-2018.
 */

public class AppService extends BaseService {

    private DownloadManager downloadManager;
    private String _tabPhysicianSettings="physician";
    private String _tabPhysicianIdSettings="physicianid";
    private String _tabAgencySettings="agency";
    private String _tabFingerPrintSettings="fingerprint";
    private String _tabsignatureSettings="signature";
    private String _tabCurrentDocSettings="currentdoc";


    private String _tabSyncModeSettings="syncmode";


    private String _tabIdSettings="TabId";
    private String _tabModeSettings="TabMode";
    private String _tokenSettings="token";
    private String _tabModePhysician="1";
    private String _tabModeMarketer="0";
    private AppInfo _appinfo;
    private ApiService apiService;
    private String autoUpdateUrl="";


    public AppService(Context context){
        super(context);
        apiService=new ApiService();
    }
    public AppInfo GetRemoteAppInfo(){
        if(_appinfo==null){
            //Dowork
        }
        return _appinfo;
    }
    public boolean ssUpdateAvailable(){
        AppInfo appinfo=GetRemoteAppInfo();
        if(appinfo!=null ){
            return true;
        }
        return  false;

    }
    public long GetTabId(){
       Settings sett= db.SettingsDao().findByName(_tabIdSettings);
       if(sett!=null && sett.getValue()!=null)
           return Long.parseLong( sett.getValue());
       return 0;
    }
    private void SetFingerPrint(String fingerPrint){
        SetSettings(_tabFingerPrintSettings,fingerPrint);
    }

    public String getFingerPrint(){
        Settings settings=db.SettingsDao().findByName(_tabFingerPrintSettings);
        if(settings!=null)
            return settings.getValue();
        else
            return "";
    }
    private Settings SetSettings(String name,String value){
        Settings set=db.SettingsDao().findByName(name);
        if(set!=null)
        {
            db.SettingsDao().Update(name,value);
            set.setValue(value);
        }
        else {
            set=new Settings();
            set.setName(name);
            set.setValue(value);
            db.SettingsDao().Add(set);
        }
        return set;
    }
    public String SetTabId(String id){
        Settings settings=SetSettings(_tabIdSettings,id);
        return id;
    }
    public Boolean SaveSignature(long physicianid , AuthToken token,long tabId,String fingerPrint){

        String signature=getSignature();
        String authheader=GetAuthHeader(token.AccessToken);
        try{
            Result<Boolean>  res= apiService.SaveSignature(authheader,physicianid,tabId,fingerPrint,signature);
            if(res!=null && res.isSuccess())
                return true;
            else
                return false;
        }catch(Exception ex){
            return false;
        }
    }

    public Boolean Sign( AuthToken token,long tabId,String fingerPrint,long docId,Date signdate){

        String authheader=GetAuthHeader(token.AccessToken);
        try{
            Result<Boolean>  res= apiService.Sign(docId,signdate,tabId,fingerPrint,authheader);
            if(res!=null && res.isSuccess())
                return true;
            else
                return false;
        }catch(Exception ex){
            return false;
        }
    }
    public Boolean Reject(long docId, String comment , Date rejectdate,AuthToken token,long tabId,String fingerPrint){

        String authheader=GetAuthHeader(token.AccessToken);
        try{
            Result<Boolean>  res= apiService.Reject(docId,rejectdate,tabId,fingerPrint,authheader,comment);
            if(res!=null && res.isSuccess())
                return true;
            else
                return false;
        }catch(Exception ex){
            return false;
        }
    }
    public Boolean SignUp(long physicianid , AuthToken token,long tabId,String fingerPrint,String contact){

        String authheader=GetAuthHeader(token.AccessToken);
        try{
            Result<Boolean>  res= apiService.SignUp(authheader,physicianid,tabId,fingerPrint,contact);
            if(res!=null && res.isSuccess())
                return true;
            else
                return false;
        }catch(Exception ex){
            return false;
        }
    }
    public void setCurrentDocument(int idx){
        SetSettings(_tabCurrentDocSettings, idx+"");

    }
    public int getCurrentDocument(){

        Settings sett=db.SettingsDao().findByName(_tabCurrentDocSettings);
        if(sett==null){
            return 0;
        }
        else
            return Integer.parseInt(sett.getValue());


    }
    public void CompleteDocumentSignSync(){
        setNormalMode();
    }
    public boolean isRegistered(){
        return GetTabId()>0;
    }
    public Result<AuthToken> ValidateUser(String username,String password){
        AuthToken authToken=apiService.GetAuthToken(username,password);
        Result<AuthToken> result=new Result<AuthToken>();
        if(authToken.isValid()){
            Result<UserModel> me=getMe(authToken);
            if(me!=null && me.isSuccess()){
                authToken.setCompany(me.getValue().getCompany());
                result.setSuccess(true);
                result.setValue(authToken);
                SignIn(authToken);
            }
            else{
                result.setSuccess(false);
                result.setErrorMessage("User is not active");
            }

        }
        else{
            result.setSuccess(false);
            result.setErrorMessage(authToken.getError());
        }
        return result;
    }
    private void SignIn(AuthToken token){
        SetSettings(_tokenSettings,token.getAccessToken());
        SetSettings(_tabModeSettings,_tabModeMarketer);
    }
    public String getSignature(){
        Settings sett=db.SettingsDao().findByName(_tabsignatureSettings);
        if(sett!=null)
            return  sett.getValue();
        return null;
    }
    public void setSignature(String signatureBase64){

      SetSettings(_tabsignatureSettings,signatureBase64);
    }
    public List<PhysicianInfo> getPhysicians(AuthToken token){
        String auth=GetAuthHeader(token.AccessToken);

        try{
            List<PhysicianInfo> result=apiService.GetPhysicians(auth).getValue();

            return result;
        }catch(Exception ex){
            List<PhysicianInfo> t=new ArrayList<PhysicianInfo>();
            return  t;
        }
    }
    public Result<long[]> getDocumentIds(AuthToken token,long physicianid){
        String auth=GetAuthHeader(token.AccessToken);
        Result<long[]> result=apiService.GetDocumentIds(auth,physicianid);
        return  result;
    }
    public Result<DocumentModel> getDocument(AuthToken token, long docId){
        String auth=GetAuthHeader(token.AccessToken);
        Result<DocumentModel> result=apiService.GetDocument(auth,docId);
        return  result;
    }
    public Result<UserModel> getMe(AuthToken token){
        String auth=GetAuthHeader(token.AccessToken);
        Result<UserModel> result=apiService.GetMe(auth);
        return  result;
    }
    public Result<TabletInfo> register(String imei,String model,String mac,String brand,String username,String password){
        TabletInfo tablet=new TabletInfo();
        tablet.setBrand(brand);
        tablet.setIMEI(imei);
        tablet.setMacAddress(mac);
        tablet.setModel(model);
        tablet.setName("");
        tablet.setSerial("");
        try{
            Result<TabletInfo> result=apiService.RegisterTablet(tablet,username,password);

            if(result.isSuccess()){
                SetTabId(result.getValue().getId()+"");
                db.SettingsDao().Update("TabId",result.getValue().getId()+"");
                SetFingerPrint(result.getValue().getFingerPrint());
            }

            return result;
        }catch(Exception ex){
            Result<TabletInfo> t=new Result<TabletInfo>(ex.getMessage());
           return  t;
        }
    }
    private Result<AppInfo> GetVersionInfo(){
        try{
            Result<AppInfo> result=apiService.GetVersionInfo();
            return result;
        }catch(Exception ex){
            Result<AppInfo> t=new Result<AppInfo>(ex.getMessage());
            return  t;
        }
    }
    public boolean isUpdateAvailable(){


        Result<AppInfo> tabResult=GetVersionInfo();
        AppInfo tab;
        if(tabResult!=null && tabResult.isSuccess()){
            tab=tabResult.getValue();
            if(tab.getVersion() !=null && !tab.getVersion().trim().equals("")){
                if(!AppSettings.getAppVersion().equals(tab.getVersion()))
                {
                    autoUpdateUrl=tab.getAppURL();
                    return true;
                }

            }
        }
        return false;

    }
    public String getDownloadUrl(){


       return autoUpdateUrl;

    }
   public Boolean isPhsicianLogin(){
       Settings sett= db.SettingsDao().findByName(_tabModeSettings);
       if(sett!=null && sett.getValue().equals( _tabModePhysician))
           return true;
       else
           return false;
   }
   public Boolean isKioskEnabled(Context c){

       Boolean ret=true;
       try{
          return !UnlockUtil.ShouldUnlock(c) || isPhsicianLogin();
       }
       catch(Exception ex){
            ret=isPhsicianLogin();
       }
        return ret;
   }
    public void setPhysicianMode(String PhysicianName,String Agency,long PhysicianId){
        SetSettings(_tabPhysicianSettings,PhysicianName);
        SetSettings(_tabPhysicianIdSettings,Long.toString(PhysicianId));
        SetSettings(_tabAgencySettings,Agency);
        SetSettings(_tabModePhysician,_tabPhysicianSettings);
        SetSettings(_tabModeSettings,_tabModePhysician);
        setCurrentDocument(0);
    }
    public void setSyncMode(){
        SetSettings(_tabSyncModeSettings,"1");
    }
    public void CompletePhysicingSign(String signaturebase64){
        setSignature(signaturebase64);
        setSyncMode();
        setMarketerMode();
        setCurrentDocument(0);
    }
    public void CompletePhysicianSignSync(){
        setNormalMode();
        removeElectronically();
    }
    public void SetSendDocElectronically(String contact){
        SetSettings("sendelec","1");
        SetSettings("contact",contact);


    }
    public String GetContact(){
        Settings sett=db.SettingsDao().findByName("contact");
        if(sett!=null)
            return sett.getValue();
        return "";
    }
    public Boolean isSentDocElectronically(){
        Settings sett= db.SettingsDao().findByName("sendelec");
        if(sett!=null && sett.getValue().equals("1") )
            return true;
        else
            return false;
    }
    public void removeElectronically(){
        SetSettings("sendelec","0");
    }
    public void setNormalMode(){
        SetSettings(_tabSyncModeSettings,"0");
        db.SettingsDao().DeleteByName(_tabPhysicianSettings);
        db.SettingsDao().DeleteByName(_tabPhysicianIdSettings);
        db.SettingsDao().DeleteByName(_tabsignatureSettings);
        db.SettingsDao().DeleteByName("sendelec");

    }
    public Boolean isSyncMode(){
        Settings sett= db.SettingsDao().findByName(_tabSyncModeSettings);
        if(sett!=null && sett.getValue().equals( "1"))
            return true;
        else
            return false;
    }
    public PhysicianInfo getSyncPhysician(){
        Settings settname= db.SettingsDao().findByName(_tabPhysicianSettings);
        Settings settid= db.SettingsDao().findByName(_tabPhysicianIdSettings);
        PhysicianInfo info=new PhysicianInfo();
        info.Name=settname.getValue();
        info.Id=Long.parseLong(settid.getValue());
        return info;
    }
    public void setMarketerMode(){
        SetSettings(_tabModeSettings,_tabModeMarketer);
    }
    public String getTabPhysician(){
        Settings sett= db.SettingsDao().findByName(_tabPhysicianSettings);
        if(sett!=null){
            return sett.getValue();
        }
        return "";
    }
    public String getTabPAgency(){
        Settings sett= db.SettingsDao().findByName(_tabAgencySettings);
        if(sett!=null){
            return sett.getValue();
        }
        return "";
    }
    public long getTabPhysicianId(){
        Settings sett= db.SettingsDao().findByName(_tabPhysicianIdSettings);
        if(sett!=null){
            return Long.parseLong(sett.getValue());
        }
        return 0;
    }

    public ItemModel getLoggedInPhysician(){
       return new ItemModel(0,"");
    }
}
