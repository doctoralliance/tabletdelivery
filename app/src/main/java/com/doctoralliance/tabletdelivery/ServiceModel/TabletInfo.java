package com.doctoralliance.tabletdelivery.ServiceModel;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ansad on 01-11-2018.
 */

public class TabletInfo {
    @SerializedName("id")
    public long Id;

    @SerializedName("imei")
    public String IMEI;

    @SerializedName("name")
    public String Name;

    @SerializedName("brand")
    public String Brand;

    @SerializedName("model")
    public String Model;

    @SerializedName("serial")
    public String Serial;

    @SerializedName("macAddress")
    public String MacAddress;

    @SerializedName("appVersion")
    public String AppVersion;

    @SerializedName("pinned")
    public Boolean Pinned;

    public String getFingerPrint() {
        return FingerPrint;
    }

    public void setFingerPrint(String fingerPrint) {
        FingerPrint = fingerPrint;
    }

    @SerializedName("fingerPrint")
    public String FingerPrint;

    public void setId(long id) {
        Id = id;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public void setModel(String model) {
        Model = model;
    }

    public void setSerial(String serial) {
        Serial = serial;
    }

    public void setMacAddress(String macAddress) {
        MacAddress = macAddress;
    }

    public void setAppVersion(String appVersion) {
        AppVersion = appVersion;
    }

    public void setPinned(Boolean pinned) {
        Pinned = pinned;
    }


    public long getId() {
        return Id;
    }

    public String getIMEI() {
        return IMEI;
    }

    public String getName() {
        return Name;
    }

    public String getBrand() {
        return Brand;
    }

    public String getModel() {
        return Model;
    }

    public String getSerial() {
        return Serial;
    }

    public String getMacAddress() {
        return MacAddress;
    }

    public String getAppVersion() {
        return AppVersion;
    }

    public Boolean getPinned() {
        return Pinned;
    }
}
