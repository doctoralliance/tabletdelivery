package com.doctoralliance.tabletdelivery;

import android.Manifest;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.doctoralliance.tabletdelivery.Service.AppService;
import com.doctoralliance.tabletdelivery.ServiceModel.AuthToken;
import com.doctoralliance.tabletdelivery.ServiceModel.PhysicianInfo;
import com.doctoralliance.tabletdelivery.ServiceModel.Result;
import com.doctoralliance.tabletdelivery.ServiceModel.TabletInfo;
import com.doctoralliance.tabletdelivery.Settings.AppSettings;
import com.doctoralliance.tabletdelivery.Utils.KioskUtil;
import com.doctoralliance.tabletdelivery.Utils.NetworkUtil;

import org.w3c.dom.Text;

import java.util.List;
import java.util.UUID;

import static com.doctoralliance.tabletdelivery.Utils.KioskUtil.DisableKioskMode;

public class LoginActivity extends AppCompatActivity {
    AppService appService=null;

    String type="1";
    TextView txtVersion;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {

                CommonUtil.ShowError(LoginActivity.this,paramThrowable);
            }
        });

        try{
        appService=new AppService(getApplicationContext());
       //appService.SetTabId("0");
        Intent intent = getIntent();
        type = intent.getStringExtra("type").toString();


        setContentView(R.layout.activity_login);

        KioskUtil.Init(LoginActivity.this);
        DisableKioskMode(LoginActivity.this);
        InitLogin(type);
        TextView changeSSID=(TextView)findViewById(R.id.txtChangeSSID);
        txtVersion=(TextView)findViewById(R.id.txtVersion);
        txtVersion.setText("Version:" + AppSettings.getAppVersion());


        changeSSID.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                //StartWifiSettings();
                StartWifiIntent();
            }
        });

        InitWifi();
        }
        catch (Exception ex){
            CommonUtil.ShowError(LoginActivity.this,ex);
        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        KioskUtil.hideSystemUI(getWindow().getDecorView());
        InitWifi();
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        KioskUtil.hideSystemUI(getWindow().getDecorView());
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        KioskUtil.hideSystemUI(getWindow().getDecorView());
                    }
                });
    }
    @Override
    public void onBackPressed() {

    }
    public void OnPresentationClick(View v){
        Intent wifiIntent = new Intent(LoginActivity.this,PresentationActivity.class);

        //wifiIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity( wifiIntent);
    }
    public void StartWifiSettings(){
        Intent wifiIntent = new Intent(LoginActivity.this,WifiActivity.class);
        wifiIntent.putExtra("type",type);
        //wifiIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity( wifiIntent);
    }
    public void StartWifiIntent(){
        Intent gpsOptionsIntent = new Intent(  android.provider.Settings.ACTION_WIFI_SETTINGS);
        startActivityForResult(gpsOptionsIntent,0);
    }
    public void InitWifi(){
        String currentWifi= NetworkUtil.getWifiName(LoginActivity.this);
        String wifiCaption="WiFi Network: ";
        if(currentWifi==null){
            wifiCaption +="(None)";
        }
        else{
            wifiCaption +=currentWifi;
        }
        ((TextView)findViewById(R.id.txtCurrentSSID)).setText(wifiCaption);
    }
    public void OnLoginClick(View v){
        if(type.equals("0"))
            Register();
        else
            Login();
    }
    public void InitLogin(String t){

        EditText username=((EditText)findViewById(R.id.register_username));
        EditText password=((EditText)findViewById(R.id.register_password));
        type=t;
        password.setText("");
        username.setText("");

        if(type.equals("1")){
            setTitle("Login");
        }else{
            setTitle("Register Tablet");
        }

    }
    public void setTitle(String title){
        TextView text=(TextView)findViewById(R.id.app_title);
        text.setText(title);
    }
    public void doRegister(){
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                    WifiManager wimanager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    String macAddress = wimanager.getConnectionInfo().getMacAddress();

                    String username=((EditText)findViewById(R.id.register_username)).getText().toString().trim();
                    String password=((EditText)findViewById(R.id.register_password)).getText().toString().trim();
                    if(username!="" && password!=""){

                        Result<TabletInfo> tabletResult=appService.register(GetDeviceId(telephonyManager),Build.MODEL, macAddress,Build.MANUFACTURER,username,password);
                        if(tabletResult.isSuccess()){
                            Intent login=new Intent(LoginActivity.this,LoginActivity.class);
                            login.putExtra("type","1");
                            startActivity(login);
                        }
                        else{
                            showToast( tabletResult.getErrorMessage());
                        }
                    }
                } catch (Exception e) {
                    showToast( e.getMessage());

                }
            }
        });

        thread.start();


    }
    public String GetDeviceId(TelephonyManager telephonyManager){

        String dId =telephonyManager.getDeviceId();
        if(dId==null || dId.equals("")){
            String androidId = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            return androidId;
        }
        else
            return dId;
    }
    public void showToast(final String toast)
    {
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText(LoginActivity.this, toast, Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == 1) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                doRegister();
            }
        }
    }
    public void Register(){
        if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_PHONE_STATE ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this,new String[]{Manifest.permission.READ_PHONE_STATE }, 1);
        }
        else{
        doRegister();
        }
    }


public void StartSelectionIntent(AuthToken token){
    Intent seletionIntent = new Intent(LoginActivity.this,SelectionActivity.class);
    seletionIntent.putExtra("auth",token);
    //wifiIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity( seletionIntent);

}
    public void Login(){
       // String a=appService.getFingerPrint();
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                String username=((EditText)findViewById(R.id.register_username)).getText().toString().trim();
                String password=((EditText)findViewById(R.id.register_password)).getText().toString().trim();
                Result<AuthToken> token= appService.ValidateUser(username,password);
                if(!token.isSuccess())
                {
                    showToast(token.getErrorMessage());
                }
                else{
                    StartMarketerLogin(token.getValue());
                }
            }
        });
        thread.start();
    }
    public void StartMarketerLogin(AuthToken token){

        if(appService.isSyncMode()){
            Intent syncIntent=new Intent(LoginActivity.this,SignReviewActivity.class);
            //syncIntent.putExtra("synctype","1");
            syncIntent.putExtra("auth",token);
           // syncIntent.putExtra("physician",appService.getSyncPhysician());
            startActivity(syncIntent);
        }
        else{

            if(appService.isUpdateAvailable()){
                Intent updateIntent=new Intent(LoginActivity.this,AutoUpdateActivity.class);
                //syncIntent.putExtra("synctype","1");
                updateIntent.putExtra("url",appService.getDownloadUrl());
                // syncIntent.putExtra("physician",appService.getSyncPhysician());
                startActivity(updateIntent);
            }
            else {
                StartSelectionIntent(token);
            }

        }
    }
}
