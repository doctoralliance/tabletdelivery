package com.doctoralliance.tabletdelivery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.doctoralliance.tabletdelivery.Service.AppService;
import com.doctoralliance.tabletdelivery.ServiceModel.AuthToken;
import com.doctoralliance.tabletdelivery.ServiceModel.PhysicianInfo;
import com.doctoralliance.tabletdelivery.Settings.AppSettings;
import com.doctoralliance.tabletdelivery.Sync.DocumentProgressResult;
import com.doctoralliance.tabletdelivery.Sync.DocumentPullService;
import com.doctoralliance.tabletdelivery.Utils.KioskUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import pl.droidsonroids.gif.GifImageView;

public class SyncActivity extends AppCompatActivity {

    AppService appService;
    AuthToken auth;
    PhysicianInfo currentPhysician;
    private DocumentServiceBroadcastReceiver documentBroadcastReceIver;
    TextView documentsCount;
    TextView documentprogress;
    Button deliverTo;
    Intent intentMyIntentService;
    RelativeLayout selectction_transfet;
    RelativeLayout selectction_confirm;
    TextView txtFrom;
    TextView txtTo;
    TextView txtDate;
    String syncType="";
    TextView txtSyncHeader;
    LinearLayout layoutBackToPhysician;
    Button btnCancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);
        KioskUtil.Init(SyncActivity.this);
        Intent i=getIntent();
        auth=(AuthToken) i.getSerializableExtra("auth");
        currentPhysician=(PhysicianInfo)  i.getSerializableExtra("physician");
        syncType=i.getStringExtra("synctype");
        documentsCount=(TextView)findViewById(R.id.documents_count);
        documentprogress=(TextView)findViewById(R.id.documents_progress);
        txtSyncHeader=(TextView)findViewById(R.id.txtSyncHeader);
        deliverTo=(Button)findViewById(R.id.button_deliver);
        selectction_transfet=(RelativeLayout)findViewById(R.id.selectction_transfet);
        selectction_confirm=(RelativeLayout)findViewById(R.id.selectction_confirm);
        txtFrom=(TextView)findViewById(R.id.txtFrom);
        txtTo=(TextView)findViewById(R.id.txtTo);
        txtDate=(TextView)findViewById(R.id.txtdate);
        layoutBackToPhysician=(LinearLayout)findViewById(R.id.layoutBackToPhysician);
        btnCancel=(Button)findViewById(R.id.btnCancel);
        InitPage();

        appService=new AppService(getApplicationContext());
        RegisterReceiver();

        StartDocService(auth,currentPhysician.Id);
        
    }
    private void StartDocService(AuthToken token,long physicianId){
        //Start MyIntentService
        intentMyIntentService = new Intent(this, DocumentPullService.class);
        intentMyIntentService.putExtra("synctype", syncType);
        intentMyIntentService.putExtra("token", token);
        intentMyIntentService.putExtra("Physician", currentPhysician);
        startService(intentMyIntentService);
    }
    private void InitPage(){

        setTitle(auth.getCompany() +" Tablet Delivery") ;
        TextView physician=(TextView)findViewById(R.id.physician_name);
        physician.setText(currentPhysician.Name);
        deliverTo.setVisibility(View.GONE);
        deliverTo.setText("Deliver To " + currentPhysician.Name);
        selectction_transfet.setVisibility(View.GONE);
        selectction_confirm.setVisibility(View.VISIBLE);
        txtFrom.setText(auth.getCompany());
        txtTo.setText(currentPhysician.Name);

        SimpleDateFormat sdf=new SimpleDateFormat("MMM dd, yyyy hh:mm:ss");
        String dateString=sdf.format(Calendar.getInstance().getTime());

        txtDate.setText(dateString);

        if(syncType.equals("1")){
            txtSyncHeader.setText("Syncing Signed Documents For");
            btnCancel.setVisibility(View.GONE);
        }
    }
    public void setTitle(String title){
        TextView t=(TextView)findViewById(R.id.app_title);
        t.setText(title);
        TextView t1=(TextView)findViewById(R.id.app_title1);
        t1.setText(title);
    }
    private void RegisterReceiver(){
        documentBroadcastReceIver=new DocumentServiceBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppSettings.SERVICE_ACTION.FETCH.toString());
        intentFilter.addAction(AppSettings.SERVICE_ACTION.ERROR.toString());
        intentFilter.addAction(AppSettings.SERVICE_ACTION.COMPLETED.toString());
        intentFilter.addAction(AppSettings.SERVICE_ACTION.DOCUMENT.toString());
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(documentBroadcastReceIver, intentFilter);
    }
    @Override
    protected void onResume() {
        super.onResume();
        KioskUtil.hideSystemUI(getWindow().getDecorView());
    }

    @Override
    public void onBackPressed() {

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //un-register BroadcastReceiver
        Dispose();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //un-register BroadcastReceiver
        //Dispose();


    }
    private void Dispose(){
        try{
            unregisterReceiver(documentBroadcastReceIver);
        }
        catch (Exception ex){

        }

    }
    public void showToast(final String toast)
    {
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText(SyncActivity.this, toast, Toast.LENGTH_LONG).show();
            }
        });
    }
    public void UpdateProgress(DocumentProgressResult result){
        documentsCount.setText("Documents: "+ result.getCurrent() +" / "+result.getTotal());
        documentprogress.setText("Progress: "+ result.getPercentage() +"%");
    }
    public void SetDownloadCompleted(DocumentProgressResult result){
        if(result.getTotal()==0 || result.getCurrent()>0){
            GifImageView img=(GifImageView)findViewById(R.id.img_progress);
            img.setImageResource(R.drawable.checked);
           if(syncType.equals("1")){
                appService.CompletePhysicianSignSync();
               layoutBackToPhysician.setVisibility(View.VISIBLE);
           }
           else{

               deliverTo.setVisibility(View.VISIBLE);
           }

        }
        else{
            showToast("Zero files downloaded");
        }

    }
    public void onCancelClick(View v){
        stopService(intentMyIntentService);
       Intent selection=new Intent(SyncActivity.this,SelectionActivity.class);
       selection.putExtra("auth",auth);
       Dispose();
       startActivity(selection);
    }
    public void onTransferClick(View v){
        selectction_transfet.setVisibility(View.VISIBLE);
        selectction_confirm.setVisibility(View.GONE);
    }
    public void onTransferCancelClick(View v){
        selectction_transfet.setVisibility(View.GONE);
        selectction_confirm.setVisibility(View.VISIBLE);
    }
    public void onAcceptClick(View v){

        appService.setPhysicianMode(currentPhysician.Name,auth.getCompany(),currentPhysician.Id);
        if(appService.isKioskEnabled(SyncActivity.this)){
            KioskUtil.EnableKioskMod(SyncActivity.this);
        }

        Dispose();
        Intent physicianIntent=new Intent(SyncActivity.this,PhysicianActivity.class);
        startActivity(physicianIntent);
    }

    public void ShowCancel(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getApplicationContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getApplicationContext());
        }
        builder.setTitle("Cancel Sync")
                .setMessage("Are you sure you want to cancel this activity?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        intentMyIntentService.putExtra("action","clean");
                        stopService(intentMyIntentService);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    public void ProcessUpdate(DocumentProgressResult result, AppSettings.SERVICE_ACTION action){

        if(result!=null){
            switch (action) {
                case COMPLETED: { //<-- error on this line
                    SetDownloadCompleted(result);
                    break;
                }

                case FETCH: { //<-- error on this line
                    UpdateProgress(result);
                    break;
                }

                case ERROR: { //<-- error on this line
                    showToast(result.getMessage());
                    break;
                }

                case DOCUMENT: { //<-- error on this line
                    UpdateProgress(result);
                    break;
                }
                case CLEAN: { //<-- error on this line
                    //UpdateProgress(result);
                    break;
                }
            }
        }


    }
    public class DocumentServiceBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            DocumentProgressResult result= (DocumentProgressResult)intent.getSerializableExtra("Result");
            AppSettings.SERVICE_ACTION action =AppSettings.SERVICE_ACTION.valueOf(intent.getAction());
            ProcessUpdate(result,action);
        }
    }
}
