package com.doctoralliance.tabletdelivery.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doctoralliance.tabletdelivery.R;
import com.doctoralliance.tabletdelivery.ServiceModel.PhysicianInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ansad on 01-15-2018.
 */

public class PhysicianAdapter extends ArrayAdapter<PhysicianInfo> {
    int resource;
    String response;
    Context context;
    //Initialize adapter
    public PhysicianAdapter(Context context,  List<PhysicianInfo> items) {
        super(context, 0, items);
        this.resource=resource;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // Get the data item for this position
        PhysicianInfo phys = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.label);

        // Populate the data into the template view using the data object
        tvName.setText(phys.Name + " ("+ phys.NoOfDocuments +")");

        // Return the completed view to render on screen
        return convertView;
    }
}
