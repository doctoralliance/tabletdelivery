package com.doctoralliance.tabletdelivery;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.TextView;


import com.doctoralliance.tabletdelivery.Core.Entity.Document;
import com.doctoralliance.tabletdelivery.Receiver.IPowerReceiverCallback;
import com.doctoralliance.tabletdelivery.Receiver.PowerConnectionReceiver;
import com.doctoralliance.tabletdelivery.Service.AppService;
import com.doctoralliance.tabletdelivery.Service.DocumentService;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import com.doctoralliance.tabletdelivery.Settings.AppSettings;
import com.doctoralliance.tabletdelivery.Utils.KioskUtil;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.shockwave.pdfium.PdfDocument;

public class PhysicianActivity extends AppCompatActivity {
    AppService appService;
    DocumentService docService;
    long physicianId;
    String physicianName;
    String agencyName;
    long totalDocs=0;
    int currentPos=-1;
    long[] docIds;
    Button btnGetStarted;
    RelativeLayout layoutIntro;
    RelativeLayout layoutViewer;
    PDFView pdfView;
    TextView txtPatient;
    TextView txtAgency;
    TextView txtDocument;
    ImageButton btnNext;
    ImageButton btnPrev;
    TextView txtComments;
    TextView txtDate;
    LinearLayout tv_footer;
    ProgressBar progressbar;
    RadioButton rdoYes;
    RadioButton rdoNo;
    Button btnCommentDone;
    Button txtComment;
    ImageButton btnHelp;
    ImageButton btnClose;
    PopupWindow  popupWindow;


    ImageButton btnHelpClose;
    PopupWindow  popupHelpWindow;


    Document currentDocument;
    Date currentSignDate;
    Button btnSignAll;
    TextView docTitle;
    TextView txtPhysician;
    TextView txtAgencyDesc;
    Calendar myCalendar;
    PowerConnectionReceiver powerReceiver;
    PopupWindow  popupPower;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_physician);

        KioskUtil.Init(PhysicianActivity.this);
        appService=new AppService(this);
        docService=new DocumentService(this);

        initPage();
        pdfView.post( new Runnable() {
            @Override
            public void run() {
                getIntentVal();
            }
        });

    }

    private void getIntentVal(){
        try {
            if(getIntent().getStringExtra("doc").equals("1")){
                btnGetStarted.performClick();
            }
        }
        catch (Exception ex){

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        KioskUtil.hideSystemUI(getWindow().getDecorView());
        if(AppSettings.hasPowerDisplayedLow==false){
            powerReceiver=new PowerConnectionReceiver(new PowerService());
            registerReceiver(powerReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        }

    }
    @Override
    protected void onPause() {
        super.onPause();
        UnregisterPowerReceiver();

    }
    private void UnregisterPowerReceiver(){
        if(powerReceiver!=null){
            unregisterReceiver(powerReceiver);
            powerReceiver=null;
        }
    }


    public void ShowBatteryPopup(int percentage){
        if(popupPower!=null)
        {
            popupPower.dismiss();
            popupPower=null;
        }

        LayoutInflater layoutInflater = (LayoutInflater) PhysicianActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.pwr_layout,null);

        ImageButton btnPowerClose = (ImageButton) customView.findViewById(R.id.ib_close);
        Button btnpowerOk = (Button) customView.findViewById(R.id.btnCommentDone);

        TextView txtHead=(TextView) customView.findViewById(R.id.txt_Head);
        txtHead.setText("Low battery warning - " + percentage +"%");

        TextView txtContent=(TextView) customView.findViewById(R.id.txt_Content);
        txtContent.setText("Battery is at "+ percentage +"%. Please connect the tablet to be charged.");
        //instantiate popup window
        popupPower = new PopupWindow(customView, RelativeLayout.LayoutParams.WRAP_CONTENT, 200,true);




        //close the popup window on button click
        btnPowerClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupPower.dismiss();
            }
        });
        //close the popup window on button click
        btnpowerOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupPower.dismiss();
            }
        });
        popupPower.showAtLocation(layoutViewer, Gravity.CENTER, 0, 0);
    }
    public class PowerService implements IPowerReceiverCallback{
        @Override
        public void Excecute(int percentage,Boolean isCharging)
        {
            if(!isCharging){
                if(percentage<= AppSettings.batteryCutOffPercentageLow && !AppSettings.hasPowerDisplayedLow){
                    AppSettings.hasPowerDisplayedLow=true;
                    AppSettings.hasPowerDisplayedMed=true;
                    AppSettings.hasPowerDisplayedHigh=true;
                    ShowBatteryPopup(percentage);
                }
                else if(percentage<= AppSettings.batteryCutOffPercentageMed && !AppSettings.hasPowerDisplayedMed){
                    AppSettings.hasPowerDisplayedMed=true;
                    AppSettings.hasPowerDisplayedHigh=true;
                    ShowBatteryPopup(percentage);
                }
                else if(percentage<= AppSettings.batteryCutOffPercentageHigh && !AppSettings.hasPowerDisplayedHigh){
                    AppSettings.hasPowerDisplayedHigh=true;
                    ShowBatteryPopup(percentage);
                }
            }

        }
    }

    @Override
    public void onBackPressed() {

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        // When the window loses focus (e.g. the action overflow is shown),
        // cancel any pending hide action. When the window gains focus,
        // hide the system UI.
        if (hasFocus) {
            delayedHide(300);
        } else {
            mHideHandler.removeMessages(0);
        }
    }
    private final Handler mHideHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            KioskUtil.hideSystemUI(getWindow().getDecorView());
        }
    };
    private void delayedHide(int delayMillis) {
        mHideHandler.removeMessages(0);
        mHideHandler.sendEmptyMessageDelayed(0, delayMillis);
    }
    private void initPage(){
        SetTitleColour();
        physicianId=appService.getTabPhysicianId();
        physicianName=appService.getTabPhysician();
        agencyName=appService.getTabPAgency();
        setTitle(agencyName + " : " + physicianName + " Documents");
        btnGetStarted=(Button)findViewById(R.id.button_Start);
        btnGetStarted.setEnabled(false);
        layoutIntro=(RelativeLayout)findViewById(R.id.introLayout);
        layoutViewer=(RelativeLayout)findViewById(R.id.document_Viewer);
        layoutViewer.setVisibility(View.GONE);
        layoutIntro.setVisibility(View.VISIBLE);
        pdfView=(PDFView)findViewById(R.id.pdf_view);

        txtPatient=(TextView)findViewById(R.id.txtPatient);
        txtAgency=(TextView)findViewById(R.id.txtAgency);
        txtDocument=(TextView)findViewById(R.id.txtDocument);
        btnNext=(ImageButton) findViewById(R.id.btnNext);
        btnPrev=(ImageButton)findViewById(R.id.btnPrev);
        txtComments=(TextView)findViewById(R.id.txtPatient);
        tv_footer=(LinearLayout)findViewById(R.id.tv_footer);
        progressbar=(ProgressBar)findViewById(R.id.progressbar);
        rdoYes=(RadioButton)findViewById(R.id.rdoYes);
        rdoNo=(RadioButton)findViewById(R.id.rdoNo);
        txtDate=(TextView)findViewById(R.id.txtDate);
        btnSignAll=(Button)findViewById(R.id.btnSignAll);
        docTitle=(TextView)findViewById(R.id.doc_title);
        txtAgencyDesc=(TextView)findViewById(R.id.txtAgencyDesc);

        txtPhysician=(TextView)findViewById(R.id.txtPhysician);
        btnHelp=(ImageButton)findViewById(R.id.btnhelp);

        txtPhysician.setText(physicianName);

        txtAgencyDesc.setText("All your documents from "+ agencyName +" are on this tablet.");
        InitCommentPopup();
        InitHelpPopup();
        InitDate();

        ///////
        new LoadDocumentIdsAsync().execute(physicianId);

    }
    public void setTitle(String title)
    {
        TextView textview=(TextView)findViewById(R.id.app_title);
        textview.setText(title);
    }
    public void onHelpClick(View v){

        ShowHelpPopup();

    }
    public void OnSignAllClick(View v){
        UpdateStatus();
        StartReview();

    }
    private void StartViewer(){
        layoutViewer.setVisibility(View.VISIBLE);
        layoutIntro.setVisibility(View.GONE);
        currentPos=appService.getCurrentDocument()-1;
        if(currentPos<0){
            ShowHelpPopup();
        }
        Next();
    }
    public void onGetStarted(View v){
        StartViewer();
    }
    private void Next(){
        if(currentPos<(totalDocs-1)){
            UpdateStatus();
            currentPos=currentPos+1;
            LoadDocument(docIds[currentPos]);
        }
        else if(totalDocs>0){
            btnSignAll.performClick();
        }
    }
    private void Prev(){
        if(currentPos>0){
            UpdateStatus();
            currentPos=currentPos-1;
            LoadDocument(docIds[currentPos]);
        }
    }
    private String GetSignTIME(long time){
        String dateString="";
        SimpleDateFormat sdf=new SimpleDateFormat("MMM dd, yyyy");
        if(time>0){
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            dateString=sdf.format(calendar.getTime());
        }
        else{

            dateString=sdf.format(Calendar.getInstance().getTime());
        }
        return dateString;
    }
    private void ShowPdf(String path){
        File pdfFile = new File(path);
        if(pdfFile.exists()){
            pdfView.fromFile(pdfFile).enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(true)
                    .defaultPage(0)
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .password(null)
                    .scrollHandle(null)
                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    .spacing(0)
                    .pageFitPolicy(FitPolicy.WIDTH)
                    .onError(new OnErrorListener() {
                        @Override
                        public void onError(Throwable t)
                        {

                        }
                    })
                    .onPageError(new OnPageErrorListener() {
                        @Override
                        public void onPageError(int page, Throwable t)
                        {

                        }
                    })
                    .onRender(new OnRenderListener() {
                        @Override
                        public void onInitiallyRendered(int nbPages)
                        {

                        }
                    })
                    .onDraw(new OnDrawListener() {
                        @Override
                        public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage)
                        {
                        }
                    })
                    .load();
            //pdfView.destroyDrawingCache();
        }
    }
    private void LoadDocument(long id){

        tv_footer.setEnabled(false);
        progressbar.setVisibility(View.VISIBLE);
        currentDocument=docService.get(id);
        FillDetails(currentDocument);
        ShowPdf(currentDocument.getDocPath());
        progressbar.setVisibility(View.GONE);
        tv_footer.setEnabled(true);
        appService.setCurrentDocument(currentPos);
        // MarkAsViewed(id);
    }

    private void MarkAsSigned(long id, Date SignDate,String comment){

    }

    private void MarkAsRejected(long id){

    }
    private void UpdateStatus(){
        if(currentDocument!=null){
            String status="2";
            if(rdoYes.isChecked()==true)
                status="1";
            docService.UpdateStatus(currentDocument.id,currentSignDate,txtComments.getText().toString(),status);
        }
    }

    private Date GetSignDate(long time){
        Date date=new Date();
        if(time>0){
            return new Date(time);
        }
        else
            return new Date();

    }
    private void InitDate(){



        txtDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                myCalendar = Calendar.getInstance();
                myCalendar.setTime(currentSignDate);
                DatePickerDialog dpd = new DatePickerDialog(PhysicianActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int day) {
                                myCalendar.set(year, month, day);
                                String date = new SimpleDateFormat("MMM dd, yyyy").format(myCalendar.getTime());
                                txtDate.setText(date);
                                currentSignDate=myCalendar.getTime();
                            }
                        },myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH));
                dpd.getDatePicker().setMinDate(System.currentTimeMillis());

                Calendar d = Calendar.getInstance();
                d.add(Calendar.MONTH,1);

                dpd.getDatePicker().setMaxDate(d.getTimeInMillis());
                dpd.show();


            }

        });
    }

    private void FillDetails(Document doc){
        docTitle.setText("Documents " + (currentPos+1) + " of " + totalDocs);
        txtPatient.setText("Patient: "+doc.patientName);
        txtAgency.setText("Agency: "+doc.careProvider);
        txtDocument.setText(doc.docType);
        if(doc.status.equals("0") || doc.status.equals("1")){
            rdoNo.setChecked(false);
            rdoYes.setChecked(true);
        }
        else{
            rdoNo.setChecked(true);
            rdoYes.setChecked(false);
        }
        txtDate.setText(GetSignTIME(doc.signedDate));
        txtComments.setText(doc.comments+"");
        currentSignDate=GetSignDate(doc.signedDate);
    }

    public void onNextClick(View v){
        Next();
    }
    public void ShowPopup(){
        //display the popup window
        popupWindow.showAtLocation(layoutViewer, Gravity.CENTER, 0, 0);
        txtComments.requestFocus();

        //delayedHide(300);
        //KioskUtil.hideSystemUI(getWindow().getDecorView());
    }

    public void onComentsClick(View v){
        ShowPopup();
    }
    public void onRdoNoOnClick(View v){

        String existingComment=txtComments.getText().toString().trim();
        if(existingComment.equals(""))
            ShowPopup();
    }
    public void InitCommentPopup(){
        LayoutInflater layoutInflater = (LayoutInflater) PhysicianActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.comment_layout,null);

        btnClose = (ImageButton) customView.findViewById(R.id.ib_close);
        btnCommentDone = (Button) customView.findViewById(R.id.btnCommentDone);
        txtComments=(EditText)customView.findViewById(R.id.txtComment);


        //instantiate popup window
        popupWindow = new PopupWindow(customView, RelativeLayout.LayoutParams.WRAP_CONTENT, 200,true);



        //close the popup window on button click
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        //close the popup window on button click
        btnCommentDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    WindowManager wm;
    WindowManager.LayoutParams layoutParams;
    public void InitHelpPopup(){
        LayoutInflater layoutInflater = (LayoutInflater) PhysicianActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.help_layout,null);

        btnHelpClose= (ImageButton) customView.findViewById(R.id.ib_close);

        //instantiate popup window
        popupHelpWindow= new PopupWindow(customView, RelativeLayout.LayoutParams.WRAP_CONTENT, 700,true);

        popupHelpWindow.setFocusable(false);
        popupHelpWindow.setOutsideTouchable(false);




        //close the popup window on button click
        btnHelpClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //layoutParams
                popupHelpWindow.dismiss();
            }
        });

    }
    public void ShowHelpPopup(){
        //display the popup window
        popupHelpWindow.showAtLocation(layoutViewer, Gravity.CENTER, 0, 0);
        View container = (View) popupHelpWindow.getContentView().getParent();
        wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        layoutParams = (WindowManager.LayoutParams) container.getLayoutParams();
        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount = 0.85f;
        wm.updateViewLayout(container, layoutParams);

        //delayedHide(300);
        //KioskUtil.hideSystemUI(getWindow().getDecorView());
    }
    public void onPrevClick(View v){
        Prev();
    }
    private void StartReview(){
        Intent intent=new Intent(PhysicianActivity.this,ReviewActivity.class);

        startActivity(intent);
    }
private void SetTitleColour(){
   // getActionBar().setBackgroundDrawable(new ColorDrawable(getColor(R.color.greenbuttonBg)));
}
    private class LoadDocumentIdsAsync extends AsyncTask<Long, Integer, Long> {
        protected Long doInBackground(Long... physicianId) {
            docIds=docService.getDocids(physicianId[0]);
            totalDocs=docIds.length;
            return  0l;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Long result) {
            btnGetStarted.setEnabled(true);
        }
    }

}
