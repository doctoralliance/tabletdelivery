package com.doctoralliance.tabletdelivery.ServiceModel;

import android.util.Base64;

import com.google.gson.annotations.SerializedName;


import java.util.Date;

/**
 * Created by ansad on 01-16-2018.
 */

public class DocumentModel {

    @SerializedName("documentType")
    public String DocumentType;

    @SerializedName("documentId")
    public RecordIdentifier DocumentId;

    public String getDocumentType() {
        return DocumentType;
    }

    public void setDocumentType(String documentType) {
        DocumentType = documentType;
    }

    public RecordIdentifier getDocumentId() {
        return DocumentId;
    }

    public void setDocumentId(RecordIdentifier documentId) {
        DocumentId = documentId;
    }

    public RecordIdentifier getPatientId() {
        return PatientId;
    }

    public void setPatientId(RecordIdentifier patientId) {
        PatientId = patientId;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public byte[] getDocumentBuffer() {

        if(getDocumentbase64()!=null){
            byte[] buffer= Base64.decode(getDocumentbase64().getBytes(), Base64.DEFAULT);
            return buffer;
        }
        return new byte[0];
    }


    public String getCareProvider() {
        return CareProvider;
    }

    public void setCareProvider(String careProvider) {
        CareProvider = careProvider;
    }

    @SerializedName("patientId")
    public RecordIdentifier PatientId;
    @SerializedName("patientName")
    public String PatientName;

    public String getDocumentbase64() {
        return Documentbase64;
    }

    public void setDocumentbase64(String documentbase64) {
        Documentbase64 = documentbase64;
    }

    @SerializedName("documentBuffer")
    public String Documentbase64;

    @SerializedName("careProvider")
    public String CareProvider;

    public Date getSendDate() {
        return SendDate;
    }

    public void setSendDate(Date sendDate) {
        SendDate = sendDate;
    }

    @SerializedName("sendDate")
    public Date SendDate;

}
