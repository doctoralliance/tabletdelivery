package com.doctoralliance.tabletdelivery.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.doctoralliance.tabletdelivery.CommonUtil;
import com.doctoralliance.tabletdelivery.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.concurrent.ExecutionException;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

public class ImageAdapter extends PagerAdapter {

    int[] mResources = {
            R.drawable.p1,
            R.drawable.p2,
            R.drawable.p3,
            R.drawable.p4,
            R.drawable.p5,
            R.drawable.p6,
            R.drawable.p7,
            R.drawable.p8,
            R.drawable.p9,
            R.drawable.p10,
            R.drawable.p11,
            R.drawable.p12
    };

    Activity activity;
    LayoutInflater inflater;
    Context appContext;
    Object retVal;
    private float x1,x2,y1,y2;
    public  ImageAdapter(Activity act ){
        activity=act;

        //loadImages();
        appContext=activity.getApplicationContext();
        inflater =(LayoutInflater)appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {

        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        if(position>-1 && position<mResources.length){

            View itemView=inflater.inflate(R.layout.image_item,container,false);
            ImageViewTouch image=(ImageViewTouch) itemView.findViewById(R.id.slider_imageView);


            container.addView(itemView);




            try{

                Picasso.get().load(mResources[position]).into(image);

                //Picasso.get()
               //         .load("http://i.imgur.com/DvpvklR.png")
                       // .placeholder(R.drawable.ic_loading)
                       //  .error(R.mipmap.ic_launcher)
                      //  .into(image);
            }
            catch (Exception ex)
            {
                Log.d("Error",ex.getMessage());
            }

            image.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    if(event.getAction()==MotionEvent.ACTION_DOWN){

                        x1=event.getX();
                        y1=event.getY();

                    }
                    else if(event.getAction()==MotionEvent.ACTION_UP){
                        x2=event.getX();
                        y2=event.getY();
                        if(Math.abs(x1-x2) >20 || Math.abs(y1-y2) >20){}
                        else{
                            CommonUtil.ShowTitle(activity);
                        }

                    }

                    return false;

                }
            });
            return itemView;
        }
        retVal= super.instantiateItem(container,position);

        return retVal;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object );

    }

}
