package com.doctoralliance.tabletdelivery.Sync;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.doctoralliance.tabletdelivery.Core.Entity.Document;
import com.doctoralliance.tabletdelivery.REST.ApiService;
import com.doctoralliance.tabletdelivery.Service.AppService;
import com.doctoralliance.tabletdelivery.Service.DocumentService;
import com.doctoralliance.tabletdelivery.ServiceModel.AuthToken;
import com.doctoralliance.tabletdelivery.ServiceModel.DocumentModel;
import com.doctoralliance.tabletdelivery.ServiceModel.PhysicianInfo;
import com.doctoralliance.tabletdelivery.ServiceModel.Result;
import com.doctoralliance.tabletdelivery.Settings.AppSettings;

import java.util.Date;

/**
 * Created by ansad on 01-16-2018.
 */

public class DocumentPullService extends IntentService {

    PhysicianInfo Physician;
    AuthToken token;
    AppService appService;
    DocumentService docService;
    ApiService apiService;

    public DocumentPullService(){
        super("com.doctoralliance.tabletdelivery.Sync.DocumentPullService");

        appService=new AppService(DocumentPullService.this);
        docService=new DocumentService(DocumentPullService.this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //get input
        Physician=(PhysicianInfo) intent.getSerializableExtra("Physician");
        token=(AuthToken) intent.getSerializableExtra("token");


        String syncType=intent.getStringExtra("synctype");
        if(syncType.equals("0")){
            ProcessPullDocs(intent);
        }
        else
            ProcessSignSync(intent);
        stopSelf();
    }
    private void ProcessSignSync(Intent intent){
        long tabid=appService.GetTabId();
        String fingerPrint=appService.getFingerPrint();

        Boolean isSentElectronically=appService.isSentDocElectronically();

        String authHeader;
        authHeader=docService.GetAuthHeader(token.AccessToken);
        long[] docIds=docService.getDocids(Physician.Id);
        DocumentModel doc;

        if(docIds!=null && docIds.length>0){
            SendProgress(docIds.length, 0, AppSettings.SERVICE_ACTION.DOCUMENT, "");
            //String signature = appService.getSignature();
            appService.SaveSignature(Physician.Id,token,tabid,fingerPrint);
            if(isSentElectronically){
                appService.SignUp(Physician.Id,token,tabid,fingerPrint,appService.GetContact());
            }
            int cnt=0;
            for (long docId : docIds) {
                Document document =docService.get(docId);

                Boolean docResult=null;
                    if (document != null) {
                        if (document.getStatus().equals("1")) {
                            docResult = appService.Sign(token, tabid, fingerPrint, document.getDocId(), new Date(document.getSignedDate()));
                        } else if (document.getStatus().equals("2")) {
                            docResult = appService.Reject(document.getDocId(), document.getComments(), new Date(document.getSignedDate()), token, tabid, fingerPrint);
                        }
                        if (docResult == true) {
                            docService.delete(document);
                            cnt = cnt + 1;
                            SendProgress(docIds.length, cnt, AppSettings.SERVICE_ACTION.DOCUMENT, "");

                        }
                    }

            }
            SendProgress(docIds.length,cnt, AppSettings.SERVICE_ACTION.COMPLETED,"");
        }
        else{
            SendProgress(0,0, AppSettings.SERVICE_ACTION.COMPLETED,"Zero files");
        }
    }

    private void ProcessPullDocs(Intent intent){

        docService.CleanForPhysician(Physician.Id);
        long[] docIds=getDocumentIds(token,Physician.Id);
        DocumentModel doc;
        if(docIds!=null && docIds.length>0){
            int cnt=0;
            for (long docId : docIds) {

                if(docService.getByDocId(docId)==null){
                    Result<DocumentModel> document=appService.getDocument(token,docId);
                    if(document!=null && document.isSuccess()){

                        doc=document.getValue();
                        if(doc.getDocumentBuffer().length>0){

                            docService.Save(doc.getDocumentId().Id,Physician.Id,Physician.Name,doc.getPatientId().Id,
                                    doc.getPatientName(),doc.getCareProvider(),doc.getDocumentType(),doc.getSendDate(),doc.getDocumentBuffer());
                           cnt=cnt+1;
                            SendProgress(docIds.length,cnt, AppSettings.SERVICE_ACTION.DOCUMENT,"");
                        }
                    }
                }
                else{
                    cnt=cnt+1;
                    SendProgress(docIds.length,cnt, AppSettings.SERVICE_ACTION.DOCUMENT,"");

                }

            }
            SendProgress(docIds.length,cnt, AppSettings.SERVICE_ACTION.COMPLETED,"");
        }
        else{
            SendProgress(0,0, AppSettings.SERVICE_ACTION.COMPLETED,"Zero files");
        }
    }
    private long[] getDocumentIds(AuthToken token,long PhysicianId){
        Result<long[]> result=appService.getDocumentIds(token,PhysicianId);
        if(result.isSuccess()){
            long[] docIds=result.getValue();
            if(docIds.length>0){
                SendProgress(docIds.length,0, AppSettings.SERVICE_ACTION.FETCH,"");
                return docIds;
            }
            else{
                SendProgress(0,0,AppSettings.SERVICE_ACTION.COMPLETED,"No Documents to sign");
            }
        }
        else
            SendProgress(0,0,AppSettings.SERVICE_ACTION.ERROR,result.getErrorMessage());
        return null;
    }
    private void SendProgress(int total, int current, AppSettings.SERVICE_ACTION action, String message){
        DocumentProgressResult result=new DocumentProgressResult();
        result.Total=total;
        result.Current=current;

        result.setMessage(message);

        Intent intentResponse = new Intent();
        intentResponse.setAction(action.toString());
        intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
        intentResponse.putExtra("Result", result);
        sendBroadcast(intentResponse);
    }

}