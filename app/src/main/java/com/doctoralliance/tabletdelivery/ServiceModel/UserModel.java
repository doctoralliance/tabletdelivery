package com.doctoralliance.tabletdelivery.ServiceModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ansad on 01-17-2018.
 */

public class UserModel implements Serializable {
    @SerializedName("username")
    public String Username ;
    @SerializedName("firstname")
    public String Firstname ;
    @SerializedName("lastname")
    public String Lastname ;
    @SerializedName("middleInitial")
    public String MiddleInitial ;
    @SerializedName("userType")
    public String UserType ;
    @SerializedName("status")
    public String Status ;
    @SerializedName("company")
    public String Company ;

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String firstname) {
        Firstname = firstname;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }

    public String getMiddleInitial() {
        return MiddleInitial;
    }

    public void setMiddleInitial(String middleInitial) {
        MiddleInitial = middleInitial;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }
}
