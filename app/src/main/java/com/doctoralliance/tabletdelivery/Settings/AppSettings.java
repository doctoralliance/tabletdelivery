package com.doctoralliance.tabletdelivery.Settings;
import com.doctoralliance.tabletdelivery.BuildConfig;

/**
 * Created by ansad on 01-06-2018.
 */

public  class AppSettings {
    private static String ApiURL="https://api.doctoralliance.com/";
    //private static String ApiURL="https://qa-api.doctoralliance.net/";
    //private static String ApiURL="http://b9a9875f.ngrok.io/";
    private static String TokenEndPoint="";
    private static String PhysicianListEndPoint="";
    private static String getSignableEndPoint="";
    private static String getFileEndPoint="";
    private static String SubmitDocumentEndPoint="";
    public static String getAppVersion(){
        return  BuildConfig.VERSION_NAME;
    }
    public static String getApiURL(){
        return ApiURL;
    }

    public enum SERVICE_ACTION
    {
        ERROR, FETCH, DOCUMENT,COMPLETED,CLEAN
    }
    public static float batteryCutOffPercentageHigh=15;
    public static float batteryCutOffPercentageMed=10;
    public static float batteryCutOffPercentageLow=5;

    public static Boolean hasPowerDisplayedHigh=false;
    public static boolean hasPowerDisplayedMed=false;
    public static boolean hasPowerDisplayedLow=false;

}
