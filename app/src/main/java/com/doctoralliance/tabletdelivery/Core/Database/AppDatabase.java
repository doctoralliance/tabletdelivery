package com.doctoralliance.tabletdelivery.Core.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.doctoralliance.tabletdelivery.Core.DAO.DBConverters;
import com.doctoralliance.tabletdelivery.Core.DAO.DocumentDao;
import com.doctoralliance.tabletdelivery.Core.DAO.SettingsDao;
import com.doctoralliance.tabletdelivery.Core.Entity.Document;
import com.doctoralliance.tabletdelivery.Core.Entity.Settings;

@Database(entities = {Document.class, Settings.class}, version = 1)

public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

   public abstract DocumentDao DocumentDao();

    public abstract SettingsDao SettingsDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "doctoralliance-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
