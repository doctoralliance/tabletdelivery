package com.doctoralliance.tabletdelivery.ServiceModel;

/**
 * Created by ansad on 01-09-2018.
 */

public class ItemModel {
    public ItemModel(long id,String name)
    {
        this.id=id;
        this.name=name;
    }
    public long id;
    public long getId(){
        return  this.id;
    }
    public long setId(long id){
        return  this.id=id;
    }

    public String name;
    public String getName(){
        return  this.name;
    }
    public String setName(String name){
        return  this.name=name;
    }
}
