package com.doctoralliance.tabletdelivery;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.VideoView;

public class VideoActivity extends AppCompatActivity {
    VideoView videoview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        setTitle("Demo");
        LoadVideo();
    }
    private void LoadVideo(){
        VideoView videoview = (VideoView) findViewById(R.id.videoview);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.demo_video);
        videoview.setVideoURI(uri);
        videoview.start();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(videoview!=null)
        {
            videoview.stopPlayback();
        }
    }
}
