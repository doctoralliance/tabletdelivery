package com.doctoralliance.tabletdelivery.Utils;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.widget.TextView;

import com.doctoralliance.tabletdelivery.Receiver.PowerConnectionReceiver;
import com.doctoralliance.tabletdelivery.Settings.AppSettings;

import static android.content.Intent.ACTION_BATTERY_CHANGED;

/**
 * Created by ansad on 02-08-2018.
 */

public class BatteryUtil {
    Activity activity;
    PowerConnectionReceiver receiver;
    TextView[] _updateContros;
    public BatteryUtil(Activity _activity, TextView[] updatecontrols){
        activity=_activity;
        _updateContros=updatecontrols;
        receiver=new PowerConnectionReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_BATTERY_CHANGED);
        activity.registerReceiver(receiver, intentFilter);
    }
    public class PowerConnectionReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;

            int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
            boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

            //int level = status.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            //int scale = status.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            //float batteryPct = level / (float)scale;

            for (TextView txtView:_updateContros) {
                
            }

        /*
        Intent intentResponse = new Intent();
        intentResponse.setAction(action.toString());
        intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
        intentResponse.putExtra("Result", result);
        ((ContextWrapper)context).sendBroadcast(intentResponse);
        */
        }

    }
    public void Finish(){
        if(receiver!=null){
            activity.unregisterReceiver(receiver);
        }
    }
}
