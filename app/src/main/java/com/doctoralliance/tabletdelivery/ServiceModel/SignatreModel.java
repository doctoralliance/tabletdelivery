package com.doctoralliance.tabletdelivery.ServiceModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ansad on 01-22-2018.
 */

public class SignatreModel extends TabletInfo {
    public long getPhysicianId() {
        return PhysicianId;
    }

    public void setPhysicianId(long physicianId) {
        PhysicianId = physicianId;
    }

    public String getSignaturebas64() {
        return Signaturebas64;
    }

    public void setSignaturebas64(String signaturebas64) {
        Signaturebas64 = signaturebas64;
    }

    @SerializedName("physicianId")
    public long PhysicianId;

    @SerializedName("signaturebas64")
    public String Signaturebas64;

}
