package com.doctoralliance.tabletdelivery.ServiceModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ansad on 01-08-2018.
 */

public class AppInfo {
    @SerializedName("version")
    private String Version;

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getAppURL() {
        return AppURL;
    }

    public void setAppURL(String appURL) {
        AppURL = appURL;
    }

    @SerializedName("appURL")
    private String AppURL;
}
