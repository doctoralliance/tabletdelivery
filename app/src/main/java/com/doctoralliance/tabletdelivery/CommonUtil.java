package com.doctoralliance.tabletdelivery;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by ansad on 08-11-2017.
 */

public class CommonUtil {
    public static void ShowTitle(Activity activity){
        LinearLayout top=(LinearLayout) activity.findViewById(R.id.top);
        if(top.getVisibility()== View.GONE){
            top.setVisibility(View.VISIBLE);
            HideTop(activity);
        }
    }
    public static void HideTop(final Activity activity){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                LinearLayout top=(LinearLayout)activity.findViewById(R.id.top);
                top.setVisibility(View.GONE);
            }
        }, 5000);
    }
    public static void MessageBox(String method, String message, Context context)
    {


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
         builder.create();
         builder.show();
    }

    public static void ShowError(final Context context,final Throwable paramThrowable){

        try{

            String stackTrace = "";
            for (int i = 0; i < paramThrowable.getStackTrace().length; i++) {
                stackTrace += paramThrowable.getStackTrace()[i].toString() + "\n";
            }


            Throwable tmp = paramThrowable;
            int j = 0;
            while ((tmp = tmp.getCause()) != null && j < 5) {
                j++;
                stackTrace += "Coused by:\n";
                for (int i = 0; i < tmp.getStackTrace().length; i++) {
                    stackTrace += tmp.getStackTrace()[i].toString() + "\n";
                }
            }

            String deviceInfo = "";
            deviceInfo += "OS version: " + System.getProperty("os.version") + "\n";
            deviceInfo += "API level: " + Build.VERSION.SDK_INT + "\n";
            deviceInfo += "Manufacturer: " + Build.MANUFACTURER + "\n";
            deviceInfo += "Device: " + Build.DEVICE + "\n";
            deviceInfo += "Model: " + Build.MODEL + "\n";
            deviceInfo += "Product: " + Build.PRODUCT + "\n";


            final String str= paramThrowable.getMessage() + "/n" + stackTrace + "/n" + deviceInfo;
            //;

            // Log.e("Saved error:", error.mErrorMessage + "\n" + error.mStackTrace);

            new Thread() {
                @Override
                public void run() {
                    Looper.prepare();

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Application was stopped...")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // Not worked!
                                    dialog.dismiss();
                                    System.exit(0);
                                    android.os.Process.killProcess(android.os.Process.myPid());
                                }
                            })
                            .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // Not worked!
                                    dialog.dismiss();
                                    System.exit(0);
                                    android.os.Process.killProcess(android.os.Process.myPid());

                                }
                            });
                    builder.setMessage(str);
                    builder.create();
                    builder.show();


                    Looper.loop();

                }
            }.start();
        }
        catch (Exception ex){

        }
    }
}
