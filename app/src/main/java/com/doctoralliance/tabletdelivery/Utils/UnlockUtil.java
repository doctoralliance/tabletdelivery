package com.doctoralliance.tabletdelivery.Utils;

import android.content.Context;
import android.os.Environment;

import com.doctoralliance.tabletdelivery.R;

import java.io.File;
import java.io.IOException;

/**
 * Created by ansad on 01-19-2018.
 */

public class UnlockUtil {
    public static boolean ShouldUnlock(Context c) throws IOException {
        String unlockKey="";
        String systemUnlockVar=c.getResources().getString(R.string.unlock_key).toString();
        String unlockFilepath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+  c.getResources().getString(R.string.unlock_file);
        File f=new File(unlockFilepath);
        if(f.exists()){
            unlockKey=FileUtil.readFile(f);
            if(unlockKey.equals(systemUnlockVar)){
                return true;
            }
        }

        return false;
    }


}

