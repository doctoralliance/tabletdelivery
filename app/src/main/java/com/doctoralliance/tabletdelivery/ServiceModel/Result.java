package com.doctoralliance.tabletdelivery.ServiceModel;

/**
 * Created by ansad on 01-11-2018.
 */

public class Result<T> {
    public Result(){

    }
    public Result(String errormessage){
        this.setErrorMessage(errormessage);
        this.setSuccess(false);
    }
        public T value;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isSuccess;
    public String errorCode;
    public String errorMessage;



}
