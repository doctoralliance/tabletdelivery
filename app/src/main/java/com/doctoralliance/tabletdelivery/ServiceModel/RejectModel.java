package com.doctoralliance.tabletdelivery.ServiceModel;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ansad on 01-22-2018.
 */

public class RejectModel extends TabletInfo {

    @SerializedName("docId")
    public long DocId ;

    public long getDocId() {
        return DocId;
    }

    public void setDocId(long docId) {
        DocId = docId;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public Date getRejectDate() {
        return RejectDate;
    }

    public void setRejectDate(Date rejectDate) {
        RejectDate = rejectDate;
    }
    @SerializedName("comments")
    public String Comments ;
    @SerializedName("rejectDate")
    public Date RejectDate ;
}
