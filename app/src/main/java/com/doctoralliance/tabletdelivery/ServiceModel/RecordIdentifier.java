package com.doctoralliance.tabletdelivery.ServiceModel;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ansad on 01-16-2018.
 */

public class RecordIdentifier {

    @SerializedName("id")
    public @Nullable long Id;


    @Nullable
    public long getId() {
        return Id;
    }

    public void setId(@Nullable long id) {
        Id = id;
    }

    public String getExternalId() {
        return ExternalId;
    }

    public void setExternalId(String externalId) {
        ExternalId = externalId;
    }
    @SerializedName("externalId")
    public String ExternalId;


}
