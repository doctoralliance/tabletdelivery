package com.doctoralliance.tabletdelivery.ServiceModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ansad on 01-15-2018.
 */

public class PhysicianInfo implements Serializable {
    @SerializedName("id")
    public long Id ;
    @SerializedName("name")
    public String Name ;
    @SerializedName("npi")
    public String NPI ;
    @SerializedName("noOfDocuments")
    public int NoOfDocuments;
}
