package com.doctoralliance.tabletdelivery.Service;

import android.content.Context;

import com.doctoralliance.tabletdelivery.Core.Database.AppDatabase;

/**
 * Created by User on 1/6/2018.
 */

public abstract class BaseService {

    public Context getContext() {
        return context;
    }

    public Context context;

    public AppDatabase db;

    public BaseService(Context _context){
        db = AppDatabase.getAppDatabase(_context);
        context=_context;
    }
    public String GetAuthHeader(String token){
        return  "Bearer "+ token;
    }
}