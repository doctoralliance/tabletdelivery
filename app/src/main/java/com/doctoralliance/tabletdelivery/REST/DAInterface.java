package com.doctoralliance.tabletdelivery.REST;



import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

import com.doctoralliance.tabletdelivery.ServiceModel.AppInfo;
import com.doctoralliance.tabletdelivery.ServiceModel.AuthToken;
import com.doctoralliance.tabletdelivery.ServiceModel.DocumentModel;
import com.doctoralliance.tabletdelivery.ServiceModel.PhysicianInfo;
import com.doctoralliance.tabletdelivery.ServiceModel.RejectModel;
import com.doctoralliance.tabletdelivery.ServiceModel.Result;
import com.doctoralliance.tabletdelivery.ServiceModel.SignModel;
import com.doctoralliance.tabletdelivery.ServiceModel.SignUpModel;
import com.doctoralliance.tabletdelivery.ServiceModel.SignatreModel;
import com.doctoralliance.tabletdelivery.ServiceModel.TabletInfo;
import com.doctoralliance.tabletdelivery.ServiceModel.UserModel;
import com.doctoralliance.tabletdelivery.Settings.AppSettings;

import java.util.Date;
import java.util.List;

/**
 * Created by ansad on 01-06-2018.
 */

public interface DAInterface {

    @GET("/TabletDelivery/getapkinfo")
    Call<Result<AppInfo>> getAppinfo();

    @GET("/TabletDelivery/getbyimei")
    Call<Result<AppInfo>> getTabinfo(@Query("imei") String imei);

    @FormUrlEncoded
    @POST("/v1/oauth/token")
    Call<AuthToken> getAuthtoken(@Field("grant_type") String grant_type, @Field("username") String username, @Field("password") String password,@Field("auth_type") String auth_type);


    @POST("/TabletDelivery/Register")
    Call<Result<TabletInfo>> Register(@Body TabletInfo tabletModel, @Query("username") String username, @Query("password") String password);

    @GET("/TabletDelivery/getapkinfo")
    Call<Result<AppInfo>> GetVersionInfo();

    @GET("/marketer/physicians")
    Call<Result<List<PhysicianInfo>>> getPhysicians(@Header("Authorization") String authHeader);

    @GET("/marketer/documentlist")
    Call<Result<long[]>> getDocumentIds(@Header("Authorization") String authHeader,@Query("physicianId") long physicianid);

    @GET("/document/getfile")
    Call<Result<DocumentModel>> getDocument(@Header("Authorization") String authHeader, @Query("docId.id") long docId,@Query("docId.externalId") String... externalId);

    @GET("/agencyuser/me")
    Call<Result<UserModel>> getMe(@Header("Authorization") String authHeader);


    @POST("/marketer/sign")
    Call<Result<Boolean>> sign(@Header("Authorization") String authHeader, @Body SignModel model);


    @POST("/marketer/savesignature")
    Call<Result<Boolean>> saveSignature(@Header("Authorization") String authHeader,@Body SignatreModel signatureModel);

    @POST("/marketer/reject")
    Call<Result<Boolean>> reject(@Header("Authorization") String authHeader, @Body RejectModel model);

    @POST("/marketer/signup")
    Call<Result<Boolean>> signup(@Header("Authorization") String authHeader, @Body SignUpModel model);

}
