package com.doctoralliance.tabletdelivery;

import android.Manifest;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import android.app.ProgressDialog;

import com.doctoralliance.tabletdelivery.Utils.KioskUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

public class WifiActivity extends AppCompatActivity {
    ProgressDialog loading = null;
    WifiManager mainWifiObj;
    WifiScanReceiver wifiReciever;
    ListView list;
    String wifis[];
    private static final int MY_PERMISSIONS_REQUEST =1 ;
    private Boolean isUserAction=false;
    EditText pass;
    private Dialog dialog;
    private String type="";
    WifiConfiguration wifiConfig;

    /**
     * Constant used in the location settings dialog.
     */

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_wifi);
        KioskUtil.Init(WifiActivity.this);
        Intent intent = getIntent();
        type = intent.getStringExtra("type");

        setTitle("Configure Wifi");
        list=(ListView)findViewById(R.id.list);
        Button done=(Button)findViewById(R.id.wifi_done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartLogin();
            }
        });

        mainWifiObj = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if(!mainWifiObj.isWifiEnabled()){
            mainWifiObj.setWifiEnabled(true);
        }

       //wifiReciever = new WifiScanReceiver();
        //mainWifiObj.startScan();

        // listening to single list item on click
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isUserAction=false;
                // selected item
                String ssid = ((TextView) view).getText().toString();
                connectToWifi(ssid);

            }
        });
    }
private void StartLogin(){
    Intent loginIntent = new Intent(WifiActivity.this,LoginActivity.class);
    loginIntent.putExtra("type", type); //Optional parameters

        //unregisterReceiver(wifiReciever);

    startActivity(loginIntent);
}
    protected void onPause() {
        super.onPause();
        if(wifiReciever!=null)
        {
            unregisterReceiver(wifiReciever);
        }



    }
    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                       //Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        //Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(WifiActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                           // Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        //Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }
    private void getWifi() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(WifiActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {


                ActivityCompat.requestPermissions(WifiActivity.this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST);

            }
            else if (ContextCompat.checkSelfPermission(WifiActivity.this, Manifest.permission.ACCESS_FINE_LOCATION  ) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(WifiActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION  }, MY_PERMISSIONS_REQUEST);
            }
            else{

                displayLocationSettingsRequest(WifiActivity.this);

                mainWifiObj.startScan();
            }

        }else {


            mainWifiObj.startScan();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == MY_PERMISSIONS_REQUEST) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(WifiActivity.this, "permission granted", Toast.LENGTH_SHORT).show();
                getWifi();

            }else{
                Toast.makeText(WifiActivity.this, "permission not granted", Toast.LENGTH_SHORT).show();
                return;
            }

        }
    }
    class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            String action = intent.getAction();

            if(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action) ){


                List<ScanResult> wifiList = mainWifiObj.getScanResults();
                String filtered[] = new String[wifiList.size()];
                for(int i = 0; i < wifiList.size(); i++){
                    //deviceList.add(wifiList.get(i).SSID);
                    filtered[i]=wifiList.get(i).SSID;
                    if(filtered[i].equals("")){
                        filtered[i]="(Unknown)";
                    }
                }
                list.setAdapter(new ArrayAdapter<String>(getApplicationContext(),R.layout.list_item,R.id.label, filtered));

            }
            else if(action.equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)){

                SupplicantState supl_state=((SupplicantState)intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE));
                switch(supl_state){
                    case ASSOCIATED:
                        Log.i("SupplicantState", "ASSOCIATED");
                        break;
                    case ASSOCIATING:Log.i("SupplicantState", "ASSOCIATING");
                        break;
                    case AUTHENTICATING:Log.i("SupplicantState", "Authenticating...");
                        break;
                    case COMPLETED:Log.i("SupplicantState", "Connected");
                        break;
                    case DISCONNECTED:Log.i("SupplicantState", "Disconnected");
                        break;
                    case DORMANT:Log.i("SupplicantState", "DORMANT");
                        break;
                    case FOUR_WAY_HANDSHAKE:Log.i("SupplicantState", "FOUR_WAY_HANDSHAKE");
                        break;
                    case GROUP_HANDSHAKE:Log.i("SupplicantState", "GROUP_HANDSHAKE");
                        break;
                    case INACTIVE:Log.i("SupplicantState", "INACTIVE");
                        break;
                    case INTERFACE_DISABLED:Log.i("SupplicantState", "INTERFACE_DISABLED");
                        break;
                    case INVALID:Log.i("SupplicantState", "INVALID");
                        break;
                    case SCANNING:Log.i("SupplicantState", "SCANNING");
                        break;
                    case UNINITIALIZED:Log.i("SupplicantState", "UNINITIALIZED");
                        break;
                    default:Log.i("SupplicantState", "Unknown");
                        break;

                }
                int supl_error=intent.getIntExtra(WifiManager.EXTRA_SUPPLICANT_ERROR, -1);
                if(supl_error==WifiManager.ERROR_AUTHENTICATING){


                    if(pass != null) {
                        pass.setEnabled(true);

                    }
                }
                else if(supl_state==SupplicantState.COMPLETED &&  isUserAction==true && dialog!=null){

                    if(mainWifiObj.getConnectionInfo().getSSID().equals(wifiConfig.SSID)){

                        dialog.dismiss();

                        StartLogin();
                    }
                    else{

                       if(pass!=null)
                            pass.setEnabled(true);


                    }
                }
            }

            /*
            if(action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)){
                NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (info.getState() == NetworkInfo.State.CONNECTED && isUserAction==true && dialog!=null) {

                    dialog.dismiss();
                    StartLogin();
                }
                else if(info.getState()==NetworkInfo.State.DISCONNECTED)
                {
                    pass.setEnabled(true);
                }

                //call your method
            }
            */





        }



    }

    private void finallyConnect(String networkPass, String networkSSID) {
        wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", networkSSID);
        wifiConfig.preSharedKey = String.format("\"%s\"", networkPass);

        // remember id
        int netId = mainWifiObj.addNetwork(wifiConfig);
        mainWifiObj.disconnect();
        mainWifiObj.enableNetwork(netId, true);
        mainWifiObj.reconnect();

        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"\"" + networkSSID + "\"\"";
        conf.preSharedKey = "\"" + networkPass + "\"";
        mainWifiObj.addNetwork(conf);


    }



    private void connectToWifi(final String wifiSSID) {
         dialog = new Dialog(this);
        dialog.setContentView(R.layout.connect);
        dialog.setTitle("Connect to Network");
        TextView textSSID = (TextView) dialog.findViewById(R.id.textSSID1);



        Button dialogButton = (Button) dialog.findViewById(R.id.okButton);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
        pass = (EditText) dialog.findViewById(R.id.textPassword);
        textSSID.setText(wifiSSID);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isUserAction=false;
                dialog.dismiss();
            }
        });

        // if button is clicked, connect to the network;
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String checkPassword = pass.getText().toString();
                if(!checkPassword.trim().equals("")){

                    isUserAction=true;
                    pass.setEnabled(false);
                    finallyConnect(checkPassword, wifiSSID);
                }


            }
        });
        dialog.show();
    }
    @Override
    protected void onResume() {
        super.onResume();
        KioskUtil.hideSystemUI(getWindow().getDecorView());
        wifiReciever =new WifiScanReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);

        registerReceiver(wifiReciever, intentFilter);
        getWifi();
    }

    @Override
    public void onBackPressed() {

    }
}
