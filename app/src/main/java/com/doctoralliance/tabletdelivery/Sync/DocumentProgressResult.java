package com.doctoralliance.tabletdelivery.Sync;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import java.io.Serializable;

/**
 * Created by ansad on 01-16-2018.
 */

public class DocumentProgressResult implements Serializable {

    public int Total;

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }

    public int getCurrent() {
        return Current;
    }

    public void setCurrent(int current) {
        Current = current;
    }

    public int Current;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String Message;
    public int getPercentage(){


        int percent=0;
        if(Total>0){
            percent=(Current * 100) / Total;
        }

        return percent;
    }
}