package com.doctoralliance.tabletdelivery.Core.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.sql.Date;

@Entity(tableName = "Document", indices = {@Index(value = {"docId"}, unique = true)})
public class Document {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "docId")
    private long docId;

    @ColumnInfo(name = "physicianName")
    private String physicianName;

    @ColumnInfo(name = "patientName")
    private String patientName;

    @ColumnInfo(name = "physicianId")
    private long physicianId;

    @ColumnInfo(name = "patientId")
    private long patientId;

    @ColumnInfo(name = "comments")
    private String comments;

    //Status(Synced,ToBeSynced,None)
    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "sentDate")
    private Date sentDate;

    @ColumnInfo(name = "signedDate")
    private Date signedDate;

    @ColumnInfo(name = "docPath")
    private String docPath;

    @ColumnInfo(name = "docType")
    private String docType;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDocId() {
        return docId;
    }

    public void setDocId(long docId) {
        this.docId = docId;
    }

    public String getPhysicianName() {
        return physicianName;
    }

    public void setPhysicianName(String physicianName) {
        this.physicianName = physicianName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public long getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(long physicianId) {
        this.physicianId = physicianId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public Date getSignedDate() {
        return sentDate;
    }

    public void setSignedDate(Date signedDate) {
        this.signedDate = signedDate;
    }

    public String getDocPath() {
        return docPath;
    }

    public void setDocPath(String docPath) {
        this.docPath = docPath;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }
}
