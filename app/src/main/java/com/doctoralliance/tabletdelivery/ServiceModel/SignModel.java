package com.doctoralliance.tabletdelivery.ServiceModel;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ansad on 01-22-2018.
 */

public class SignModel extends TabletInfo {

    public long getDocId() {
        return DocId;
    }

    public void setDocId(long docId) {
        DocId = docId;
    }

    public Date getSignDate() {
        return SignDate;
    }

    public void setSignDate(Date signDate) {
        SignDate = signDate;
    }

    @SerializedName("docId")
    public long DocId ;
    @SerializedName("signDate")
    public Date SignDate ;
}
