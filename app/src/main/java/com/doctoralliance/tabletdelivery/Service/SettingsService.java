package com.doctoralliance.tabletdelivery.Service;

import android.content.Context;

import com.doctoralliance.tabletdelivery.Core.Entity.Settings;

import java.util.List;

public class SettingsService extends BaseService {
    public SettingsService(Context context) {
        super(context);
    }

    public Settings addSettings(String name, String value) {

        Settings s = new Settings();
        s.setName(name);
        s.setValue(value);

        db.SettingsDao().Add(s);

        return s;
    }

    public List<Settings> getAll() {
        return db.SettingsDao().getAll();
    }

    public Settings getByName(String name) {
        return db.SettingsDao().findByName(name);
    }

    public void updateSettings(String name, String value) {
        db.SettingsDao().Update(name, value);
    }

    public int deleteSettings(String name) {
        return db.SettingsDao().DeleteByName(name);
    }

    public Settings setTabletId(String tabletId) {
        Settings s = db.SettingsDao().findByName("TabletId");

        if (s == null) {
            s = new Settings();
            s.setName("TabletId");
            s.setValue(tabletId);
        } else {
            s.setValue(tabletId);
        }

        return s;
    }

    public Settings getTabletId() {
        return db.SettingsDao().findByName("TabletId");
    }


    // tablet mode: null or 0 is Marketer and 1 for physician
    public Settings setTabletMode(String tabletMode) {
        Settings s = db.SettingsDao().findByName("TabletMode");

        if (s == null) {
            s = new Settings();
            s.setName("TabletMode");
            s.setValue(tabletMode);
        } else {
            s.setValue(tabletMode);
        }

        return s;
    }

    public Settings getTabletMode() {
        return db.SettingsDao().findByName("TabletMode");
    }

    public Settings setPhysicianId(String physicianId) {
        Settings s = db.SettingsDao().findByName("PhysicianId");

        if (s == null) {
            s = new Settings();
            s.setName("PhysicianId");
            s.setValue(physicianId);
        } else {
            s.setValue(physicianId);
        }

        return s;
    }

    public Settings getPhysicianId() {
        return db.SettingsDao().findByName("PhysicianId");
    }

    public Settings setPhysicianName(String physicianName) {
        Settings s = db.SettingsDao().findByName("PhysicianName");

        if (s == null) {
            s = new Settings();
            s.setName("PhysicianName");
            s.setValue(physicianName);
        } else {
            s.setValue(physicianName);
        }

        return s;
    }

    public Settings getPhysicianName() {
        return db.SettingsDao().findByName("PhysicianName");
    }
}
