package com.doctoralliance.tabletdelivery;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.doctoralliance.tabletdelivery.Adapters.ImageAdapter;
import com.doctoralliance.tabletdelivery.Service.AppService;
import com.doctoralliance.tabletdelivery.Utils.KioskUtil;
import com.doctoralliance.tabletdelivery.helper.ExtendedViewPager;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

public class PresentationActivity extends AppCompatActivity {

    private TextView mTextMessage;
    ViewPager viewPager;
    ImageAdapter adapter;
    private float x1,x2,y1,y2;
    AppService appService=null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appService=new AppService(getApplicationContext());
        setContentView(R.layout.activity_selection);
        if(appService.isPhsicianLogin()){
            KioskUtil.Init(PresentationActivity.this);
        }
        LoadGallaryView();
    }

    private void LoadGallaryView(){
        setContentView(R.layout.activity_presentation);
        ImageButton back=(ImageButton)findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    finish();
            }
        });
        //attachNavigation();
        viewPager=(ExtendedViewPager)findViewById(R.id.view_pager);
        //viewPager.setPageTransformer(true,new FadeViewPagerTransformer());

        adapter=new ImageAdapter(PresentationActivity.this);
        int offScreenPageLimit=2;
        //viewPager.setOffscreenPageLimit(offScreenPageLimit);
        viewPager.setAdapter(adapter);
        ImageViewTouch image=(ImageViewTouch) findViewById(R.id.slider_imageView);

        CommonUtil.HideTop(this);

    }
    @Override
    protected void onResume() {
        super.onResume();
        KioskUtil.hideSystemUI(getWindow().getDecorView());
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        KioskUtil.hideSystemUI(getWindow().getDecorView());
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        KioskUtil.hideSystemUI(getWindow().getDecorView());
                    }
                });
    }
    @Override
    public void onBackPressed() {

    }

}
